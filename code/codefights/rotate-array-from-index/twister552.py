def rotateArrayFromIndex(v, s, e):
    v.insert(e, v.pop(min(len(v) - 1, s)))
    return v
