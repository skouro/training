# !usr/bin/python
# [Finished in 0.0s]

# pylint edr3mo.py
# No config file found, using default configuration
#
# -------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 7.50/10, +2.50)
"""
Given a valid email address, find its domain part
"""


def find_email_domain():
    """Find the email domain"""
    email_file = open("DATA.lst", "r")
    address = email_file.read()
    split_address = address.split("@")
    domain = split_address[address.count("@")]
    print domain
# python ./edr3mo.py
# example.com
