exercises = []
newArray = []
withoutError = true
output = []

puts "Digite la cantidad de pruebas a realizar:"
number = gets.chomp.to_i

c = 0
while c < number do
  print "Digite la cadena #{c+1}:"
  exercises[c] = gets.chomp
  c += 1
end

c = 0
while c < number do
  myarray = exercises[c].split(//)
  counter = 0
  myarray.each do |position|
    case position
      when '{'
          newArray[counter] = position
          counter += 1
      when '['
          newArray[counter] = position
          counter += 1
      when '<'
          newArray[counter] = position
          counter += 1
      when '('
          newArray[counter] = position
          counter += 1
      when '}'
          newArray[counter] = position
          counter += 1
      when ']'
          newArray[counter] = position
          counter += 1
      when '>'
          newArray[counter] = position
          counter += 1
      when ')'
          newArray[counter] = position
          counter += 1
      else
        output[c] = 0
    end

  end

  sizeArray = newArray.length
  counter = 0
  while sizeArray > 1
    if withoutError
      newArray.each do |positions|
        case positions
          when '}'
            if newArray[counter - 1] == '{'
              newArray.delete_at(counter)
              newArray.delete_at(counter - 1)
              sizeArray -= 2
              counter = 0
              break
            else
              withoutError = false
              break
            end
          when ']'
            if newArray[counter - 1] == '['
              newArray.delete_at(counter)
              newArray.delete_at(counter - 1)
              sizeArray -= 2
              counter = 0
              break
            else
              withoutError = false
              break
            end
          when '>'
            if newArray[counter - 1] == '<'
              newArray.delete_at(counter)
              newArray.delete_at(counter - 1)
              sizeArray -= 2
              counter = 0
              break
            else
              withoutError = false
              break
            end
          when ')'
            if newArray[counter - 1] == '('
              newArray.delete_at(counter)
              newArray.delete_at(counter - 1)
              sizeArray -= 2
              counter = 0
              break
            else
              withoutError = false
              break
            end
        end #end case
        counter = counter + 1
      end
      if sizeArray == 0
        output[c] = 1
      elsif sizeArray == 1
        output[c] = 0
        withoutError = true
      end
    else
      output[c] = 0
      withoutError = true
      break
    end #end if withoutError
  end #end while sizeArray > 1

  c = c + 1
end #end primer while

puts output.map(&:inspect).join(" ")
