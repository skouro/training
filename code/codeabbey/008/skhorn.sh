#!/bin/bash
#
# Problem #8 Arithmetic Progression
#

# Function to deal with the progression
# According to the challenge:
# A + (A + B) + (A + 2B) + (A + 3B) + ...
function progression () {

    sequence=0
    data_array=($1)
    first_value="${data_array[0]}"
    step_size="${data_array[1]}"
    let values_accounted="${data_array[2]}-1"

    for i in $(seq 0 "$values_accounted")
    do
        let "sequence+=first_value+(step_size*i)"
    done
    
    echo "$sequence"
}

# Reading data from text
while read -r line || [[ -n "$line" ]]; 
do
    len=$(echo -n "$line" | wc -c)
    count=0
    if [[ "$len" -gt 3 ]]
    then
        output_array=$(progression "$line")
        let "count+=1"
    fi
    printf '%s ' "${output_array[@]}"
done < "$1"
