(ns tictactoe.core
  (:gen-class))

(defn -main []
  (def winflag 0)
  (def currentgame [[8 6 1 2 4 7 3 9 5] [6 4 1 5 3 2 8 9 7] [5 1 6 2 7 9 4 3 8] [1 8 7 4 3 9 6 5 2] [9 6 5 7 2 1 3 8 4] [3 2 9 7 1 6 8 4 5] [6 4 8 9 2 5 3 7 1] [9 4 1 8 3 2 5 6 7] [6 4 9 2 1 8 7 5 3] [2 5 8 9 1 6 4 3 7] [3 2 5 4 8 9 7 1 6] [1 4 3 9 7 6 2 8 5] [4 2 7 6 9 1 3 5 8]])
  (def answer (vec (replicate (count currentgame) 0)))
  (def games -1)
  (def tictactoe {:1 " " :2 " " :3 " " :4 " " :5 " " :6 " " :7 " " :8 " " :9 " "})
  
  (defn validatewin []
    ;Caso 1: X/O gana por casillas 1, 2, 3
    (if (and (= (tictactoe :1) (tictactoe :2) (tictactoe :3)) (not= (tictactoe :1) (tictactoe :2) (tictactoe :3) " "))
      (def winflag 1))
    
    ;Caso 2: X/O gana por casillas 4, 5, 6
    (if (and (= (tictactoe :4) (tictactoe :5) (tictactoe :6)) (not= (tictactoe :4) (tictactoe :5) (tictactoe :6) " "))
      (def winflag 2))
    
    ;Caso 3: X/O gana por casillas 7, 8, 9
    (if (and (= (tictactoe :7) (tictactoe :8) (tictactoe :9)) (not= (tictactoe :7) (tictactoe :8) (tictactoe :9) " "))
      (def winflag 3))
    
    ;Caso 4: X/O gana por casillas 1, 4, 7
    (if (and (= (tictactoe :1) (tictactoe :4) (tictactoe :7)) (not= (tictactoe :1) (tictactoe :4) (tictactoe :7) " "))
      (def winflag 4))
    
    ;Caso 5: X/O gana por casillas 2, 5, 8
    (if (and (= (tictactoe :2) (tictactoe :5) (tictactoe :8)) (not= (tictactoe :2) (tictactoe :5) (tictactoe :8) " "))
      (def winflag  5))
    
    ;Caso 6: X/O gana por casillas 3, 6, 9
    (if (and (= (tictactoe :3) (tictactoe :6) (tictactoe :9)) (not= (tictactoe :3) (tictactoe :6) (tictactoe :9) " "))
      (def winflag 6))
    
    ;Caso 7: X/O gana por casillas 1, 5, 9
    (if (and (= (tictactoe :1) (tictactoe :5) (tictactoe :9)) (not= (tictactoe :1) (tictactoe :5) (tictactoe :9) " "))
      (def winflag 7))
    
    ;Caso 8: X/O gana por casillas 3, 5, 7
    (if (and (= (tictactoe :3) (tictactoe :5) (tictactoe :7)) (not= (tictactoe :3) (tictactoe :5) (tictactoe :7) " "))
      (def winflag 8)))
  
  (defn printgame [move player]
    (def currentkey (keyword (str (* 1 move))))
    (def tictactoe (assoc tictactoe currentkey player))
    (println " +-----+-----+-----+")
    (println " | "(tictactoe :1) " | "(tictactoe :2) " | "(tictactoe :3) " | ")
    (println " +-----+-----+-----+")
    (println " | "(tictactoe :4) " | "(tictactoe :5) " | "(tictactoe :6) " | ")
    (println " +-----+-----+-----+")
    (println " | "(tictactoe :7) " | "(tictactoe :8) " | "(tictactoe :9) " | ")
    (println " +-----+-----+-----+"))
  
  (defn playtttoe [game]
    (def tictactoe {:1 " " :2 " " :3 " " :4 " " :5 " " :6 " " :7 " " :8 " " :9 " "})
    (def winflag 0)
    (def i 0)
    (def games (inc games))
    (def winturn -1)
    (while (== winturn -1)
      (do (println "  ")
        (if (not= (mod (inc i) 2) 0 ) (def turn "X"))
        (if (== (mod (inc i) 2) 0 ) (def turn "O"))
        (printgame (game i) turn)
        (validatewin)
        (if (not= winflag 0)(def winturn (inc i)))
        (if (not= winflag 0)(println "Victoria para" turn "en el turno " winturn))                             
        (if (not= winflag 0)(def answer (assoc answer games winturn)))         
        (def i (inc i))
        (if (and (> i 8) (== winflag 0)) (def winturn 0))))
    (if (== winturn 0)(println "Es un empate!")))
  
  (dotimes [j (count currentgame)]
    (def play (currentgame j))
    (playtttoe play))
  
  (println  "La respuesta es " answer)
  (println games))

