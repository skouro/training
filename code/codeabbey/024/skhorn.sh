#!/bin/bash
: '
  $ shellcheck skhorn.sh
'
function pseudo_generator () {

  # Grab initial value
  initial_value="$1"
  # Multiply it by itself
  mult=$(multiply "$initial_value")
  #printf "%s\n" "$mult"
  trunc=$(truncate "$mult")
  echo "$trunc"

}

function multiply () {

  value="$1"
  value=$(echo "$value" | sed "$pattern")
  mult=$((value*value))

  mult=$(printf "%08d" "$mult")

  echo "$mult"
}

function truncate () {

  array="$1"
  # Getting array len
  arrlen="${#array}"

  # The difference to extract
  let "n=$arrlen-2"
  # Extracting not last 2 values
  trunc=${array:${#array} - $n}
  # Extracting first 2 values
  trunc="${trunc:0:4}"

  echo "$trunc"
}

while read -r line || [[ -n "$line" ]];
do
  array=($line)
  len="${#array[@]}"
  iter=0
  flag=1
  pattern='s/^0*//'

  let "len=$len-1"
  if [[ "$len" -gt 1 ]];
  then
    for i in $(seq 0 "$len");
    do
      while [ "$flag" -eq 1 ];
      do
          if [[ "${pseudo_array}" -eq 0 ]];
          then

            pseudo_array+=(${array[$i]})
            let "iter+=1"
            new_seq="${array[$i]}"

          else
            # Generating the next sequence
            new_seq=$(pseudo_generator "$new_seq")
            
            # Looking if the new_seq is already in array
            # 0 is there
            # 1 is not
            seeking="$new_seq"
            in=1 
            for element in "${pseudo_array[@]}"; do
              # echo "Element $element Seeking $seeking"
              if [[ "$element" == "$seeking" ]]; then
                in=0
                break
              fi
            done

            # If in is != 0, the item is not there
            # If it is 0, it's already there
            if [[ "$in" -ne 0 ]];
            then
              pseudo_array+=($new_seq)
              let "iter+=1"
            else
              flag=0
              break
            fi
          fi
          #flag=0
      done

      unset pseudo_array
      flag=1
      array_iter+=($iter)
      iter=0
    done
  fi

  echo "${array_iter[@]}"
done < "$1"
: '
  $ ./skhorn.sh DATA.lst

  107 105 104 100 101 110 97 103 108 103 101 106 
'
