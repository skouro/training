/*
  $ g++ tizcano.cpp -o tizcano.out
  $
*/
#include<iostream>
#include<algorithm>
#include<fstream>

using namespace std;

int solver(int w[], int val[], int N, int W) {
  int grid[N+1][W+1];
  for (int i = 0; i <= N; ++i) {
    for (int j = 0; j <= W; ++j) {
      if (i==0 || j==0) {
        grid[i][j] = 0;
      }
      else if (w[i-1] <= j) {
        grid[i][j] = max(val[i-1] + grid[i-1][j-w[i-1]],  grid[i-1][j]);
      }
      else {
        grid[i][j] = grid[i-1][j];
      }
    }
  }
  return grid[N][W];
}
int main() {
  ifstream input("DATA.lst");
  cin.rdbuf(input.rdbuf());
  int N, Wmax;
  cin>>N>>Wmax;
  int w[N];
  int v[N];
  for (int i = 0; i < N; ++i) {
    cin>>w[i]>>v[i];
  }
  cout<<solver(w, v, N, Wmax)<<endl;
  return 0;
}
/*
  $ ./tizcano.out
  output:
  4289
*/
