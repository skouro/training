; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; n  number of pages to print
; x  seconds that it takes to printer 1 to print a page
; y  seconds that it takes to printer 2 to print a page
; t  total seconds elapsed since the begining
; p  number of printer pages at time 't'

; get an initial 't'
(defn getti [x y n]
  (int (/ (* n (* x y)) (+ x y)))
)

; get 'p' based on 't'
(defn getp [x y t]
  (int (+ (Math/floor (/ t x)) (Math/floor (/ t y))))
)

; find min 't' for which 'p' is just greater than 'n'
(defn doit [x y n]
  (loop [p 0 t (getti x y n)]
    (if (>= p n)
      (- t 1)
      (recur (getp x y t) (+ t 1))
    )
  )
)

; tokenize whitespace separated string into a vector of doubles
(defn stovd [wsstr]
  (let [l (map #(Double/parseDouble %) (clojure.string/split wsstr #" "))
        v (into [] l)]
    v
  )
)

; parse DATA.lst and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [v (stovd line) c (count v)]
          (if (= c 3)
            (let [x (get v 0)
                  y (get v 1)
                  n (get v 2)]
              (print (str (doit x y n) " "))
            )
          )
        )
      )
    )
    (println)
  )
)

; execute
(defn -main [& args]
  (process_file "DATA.lst")
)

; $lein run
;   318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
;   309055713 33295378 322580664 359769725 332892224 162592318 175123025
