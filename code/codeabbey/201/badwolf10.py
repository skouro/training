#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''
import math
NPOLYPOINTS = int(raw_input())
POLYPOINTS = []

for p in range(NPOLYPOINTS):
    a, b = raw_input().split()
    POLYPOINTS.append([int(a), int(b)])

NPOINTS = int(raw_input())
POINTS = []

for p in range(NPOINTS):
    a, b = raw_input().split()
    POINTS.append([int(a), int(b)])
PINDEX = 1
OUTPUT = ""
for px, py in POINTS:
    prevangle = 0
    accangle = 0
    for polx, poly in POLYPOINTS:
        Dx = polx-px
        Dy = poly-py
        angle = math.atan(float(Dy)/float(Dx))*180/math.pi
        if(Dx < 0 and Dy > 0):
            angle = 180 - abs(angle)
        if(Dx < 0 and Dy < 0):
            angle = 180 + abs(angle)
        if prevangle == 0:
            prevangle = angle
        Dangle = angle - prevangle
        if(abs(Dangle) > 180 and Dangle < 0):
            Dangle = Dangle + 360
        if(abs(Dangle) > 180 and Dangle > 0):
            Dangle = Dangle - 360
        accangle = accangle + Dangle
        prevangle = angle
    if abs(accangle) >= 359.0:
        OUTPUT += str(PINDEX)+" "
    PINDEX += 1

print OUTPUT
# pylint: disable=pointless-string-statement

'''
$ python badwolf10.py
10
10 5000
3000 9950
7000 9950
9990 5000
7000 50
5453 4819
6745 1403
7144 9311
3000 50
10 5000
12
8354 7354
8144 3649
2714 2749
9673 6106
179 7081
9202 3443
1859 8863
4940 4349
8856 9648
7692 5060
5101 2511
2052 2246

1 2 3 10 12
'''
