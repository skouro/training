s = []
r = []
pe = []
out = []

puts 'ingrese el numero de pruebas'
p = gets.chomp.to_i

c=0
(1..p).each do
  data = []
  puts "(#{c+1}) ingresa tres numeros separados por un espacio:"
  data = gets.chomp.split(" ")
  s[c] = data[0].to_f
  r[c] = data[1].to_f
  pe[c] = data[2].to_f
  c+=1;
end 

i = 0
(1..p).each do
  co = 1
  while s[i] < r[i]
    s[i] = ( s[i] * ( pe[i]/100 + 1) ).round(2)    
    if s[i] >= r[i]
        out[i] = co
        break;        
    end
    co+=1
  end
  i+=1
end

out.map(&:inspect).join(" ")
