#!/usr/bin/perl
# $perl dferrans.pl #perl has built-in linting
# use strict;  use warnings;
use strict;
use warnings;

my @array = ();
my $counter = 0;
my @finalresult = ();

foreach my $line ( <STDIN> ){
    my $answer = $line ;
    my @result = ();
    my @splitzero = ();

    if ($counter > 0){

       my @values = split(/ /, $answer);
       my $interest = $values[2]/100;
       my $goal = $values[1];
       my $seed_capital = $values[0];
       my $years = 0;
       while ($seed_capital <  $goal) {
             $seed_capital = $seed_capital * (1 + $interest );
             $years += 1;
       }

       push(@result,  $years);
       push(@finalresult,  join(" ",@result));
    }
    $counter ++;
}


print join(" ", @finalresult);
# $ perl dferrans.pl < DATA.lst
# 9 31 8 8 10 26 7 43 8 66 7 100 37 48 6 17 10 8
