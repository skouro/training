'''
➜  121 git:(sgomezatfluid) ✗ mix credo --strict sgomezatfluid.exs
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.1 seconds (0.01s to load, 0.1s running checks)
11 mods/funs, found no issues.
'''

defmodule FileReader do
  def lines(string) do
    String.split(string, "\n", trim: true)
  end

  def indiv(line) do
    inds = String.split(line, " ", trim: true)
    {{a, ""}, {b, ""}} = {Integer.parse(hd(inds)), Integer.parse(hd(tl(inds)))}
    {a, b}
  end

  def data_initial(filename) do
    file = File.read!(filename)
    file |> FileReader.lines
      |> hd
      |> indiv
  end

  def data_body(filename) do
    file = File.read!(filename)
    file  |> FileReader.lines
      |> tl |> hd
      |> String.split(" ")
  end
end

defmodule InsertionSort do
  def insert_sorted([], value, previous, _) do
    {previous ++ [value], 0}
  end

  def insert_sorted([h | t], value, previous, shifts) do
    cond do
      value < h ->
        {previous ++ [value | [h | t]], length([h|t])}
      value > h ->
        insert_sorted t, value, previous ++ [h], shifts
      value == h ->
        {previous ++ [value | [h | t]], length([h|t])}
    end
  end

  def sort_list([h | t]) do
    sort_list(t, [h], [])
  end

  def sort_list([h | t], ordered, shifts) do
    {newordered, newshifts} = insert_sorted(ordered, h, [], 0)
    sort_list t, newordered, shifts++[newshifts]
  end

  def sort_list([], ordered, shifts) do
    {ordered, shifts}
  end

end

numlist = FileReader.data_body("DATA.lst") |> Enum.map(&String.to_integer/1)

{orderedlist, shiftslist} = InsertionSort.sort_list numlist

Enum.map(shiftslist, fn num -> IO.write Integer.to_string(num) <> " " end)

'''
➜  121 git:(sgomezatfluid) ✗ elixir sgomezatfluid.exs
2 4 6 8 10 12 13 16 17 18 19 23 24 26 27 29 30 31 32 33 34 38 39 44 47 48 50 51
52 53 54 56 57 59 60 61 66 68 69 71 72 73 74 75 76 78 79 82 83 85 86 88 89 90 91
92 93 94 95 97 98 99 100 101 102 104 106 109 110 113 115 116 117 118 119 120
121 122 124 125 127 128 129 130 131 132 133 134 135 138 139 140 141 142 145 146
149 150 151 155 156 157 161 163 167 168 170 172 173 174 175 177 178 179 180 181
183 185 186 187 188 192 193 194 195 196 197 198
'''
