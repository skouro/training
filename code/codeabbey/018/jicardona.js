#!/usr/bin/env jsc
/*
$ gjslint jicardona.js
1 files checked, no errors found.
*/

var input = arguments[0];
lines = input.split('\n');

answer = [];

for (i = 1; i <= lines[0]; i++) {
    var r = 1;
    var values = lines[i].split(' ');
    var x = values[0], n = values[1];
    for (j = 0; j < n; j++) {
        r = (r + x / r) / 2;
    }
    answer.push(r);
}

print(answer.join(' '));

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
7.416198487095663  6.08276253029822  22.068076490713914 293.5188235491771
5.47722557564769   81.15417426109393 59.67411499134277  89.70507232035433
2178.9583211890717 27.53179979587241 12.755078162170646 9.110433579154478
4.898979485566356
*/
