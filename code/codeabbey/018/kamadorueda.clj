; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; do n steps of refinement
(defn doit
  ([r x n]
    (if (<= n 0.0)
      r
      (let [r (/ (+ r (/ x r)) 2)]
        (doit r x (- n 1))
      )
    )
  )
)

; tokenize whitespace separated string into a vector of doubles
(defn stovd
  ([wsstr]
    (let [l (map #(Double/parseDouble %) (clojure.string/split wsstr #" "))
          v (into [] l)]
      v
    )
  )
)

; parse file line by line and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [v (stovd line)
              c (count v)
              x (get v 0)
              n (get v 1)]
          (if (= c 2)
            (print (str (doit 1 x n) " "))
          )
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
    (println)
  )
)

; $lein run
;   7.416198487095663 6.08276253029822 22.068076490713914 293.5188235491771
;   5.47722557564769 81.15417426109393 59.67411499134277 89.70507232035433
;   2178.9583211890717 27.53179979587241 12.755078162170646 9.110433579154478
;   4.898979485566356
