/*
 * $ cargo fmt   --all     -- --check
 * $ cargo build --release
 *   Finished release [optimized] target(s) in 0.01s
 */

#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use std::io::BufRead;

struct object {
  x: f64,
  y: f64,
}

struct player {
  x: f64,
  y: f64,
  a: f64,
}

fn main() {
  // problem data
  let mut n: usize;
  let mut p: player = player {
    x: 0.0,
    y: 0.0,
    a: 0.0,
  };
  let mut objects: Vec<object> = std::vec::Vec::new();

  // load problem data from "DATA.lst"
  let mut i: u32 = 0;
  let file = std::fs::File::open("DATA.lst").unwrap();
  for it_line in std::io::BufReader::new(file).lines() {
    let line = &it_line.unwrap();
    let params: Vec<&str> = line.split(' ').collect();

    match i {
      0 => {
        n = params[0].parse::<usize>().unwrap();
        objects.reserve(n);
      }
      1 => {
        p = player {
          x: params[0].parse::<f64>().unwrap(),
          y: params[1].parse::<f64>().unwrap(),
          a: params[2].parse::<f64>().unwrap(),
        };
      }
      _ => {
        objects.push(object {
          x: params[0].parse::<f64>().unwrap(),
          y: params[1].parse::<f64>().unwrap(),
        });
      }
    }
    i += 1;
  }

  // rotate the points
  let mut nx: f64;
  let mut ny: f64;
  for mut o in &mut objects {
    o.x -= p.x;
    o.y -= p.y;
    nx = o.x * p.a.cos() + o.y * p.a.sin();
    ny = o.y * p.a.cos() - o.x * p.a.sin();
    o.x = nx;
    o.y = ny;
  }

  // print the visible points
  let sd: f64 = 0.5;
  let sw: f64 = 0.4;
  let mind: f64 = 0.5;
  let maxd: f64 = 60.0;
  let maxa: f64 = (sw / 2.0).atan2(sd);
  for o in &objects {
    let r: f64 = o.y.hypot(o.x);
    let a: f64 = o.y.atan2(o.x);

    if (r > mind) && (r < maxd) && (a.abs() < maxa) {
      print!("{} {} ", o.x, o.y);
    }
  }
  println!("");
}

/*
 * $ cargo run --release
 * 14.142135623707098 -0.000025973482372876333
 *
 */
