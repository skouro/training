let inp = [["joe clones koala"];["prozerpina dreams of zooophthalmology"];["alla begat sins"];["amanda plays with ogre"];["irina examines girls"];["teah eats girls"];["company sits on koala"];["amanda arranges integer array"];["president eats beer"];["maya passes by clothes"];["president sits on zooophthalmology"];["emma evaporates beer"];["emma eats scenes"];["joe passes by scenes"];["noah plays with clothes"];["emma clones scent"];["maya gives girls"];["government plays with alcohol"]];;
let split= Str.split (Str.regexp "");;
let letter_rep l = 
  let sl = List.sort compare l in
  match sl with
    | [] -> []
    | hd::tl -> 
      let acc,x,c = List.fold_left (fun (acc,x,c) y -> if y = x then acc,x,c+1 else (c)::acc, y,1) ([],hd,1) tl in
      (c)::acc;;

let h= ref 0.0 ;;

for j=0 to List.length inp -1 do
h:= 0.0;
let act_split = (split (List.nth (List.nth inp j) 0)) in
let rep_act = letter_rep act_split in
let frec= ref 0.0 in

    for i=0 to List.length rep_act -1 do
        frec := float_of_int (List.nth rep_act i)/.float_of_int (List.length act_split);
        h:= !h +. !frec*.(-1.0)*.(log(!frec)/.log(2.0)) ;
    done;
print_float !h;
print_string " ";
done;
