using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        private static int mayoNume = 0, menoNume = 0;
        static void Main(string[] args)
        {
            Console.WriteLine("Número de repeticiones: ");
            int repe = 0;
            string numeros = "", almacenamiento = "";
            repe = int.Parse(Console.ReadLine());
            for (int i = 0; i < repe; i++)
            {
                almacenamiento = Console.ReadLine();
                numeros = numeros + almacenamiento + " ";
            }
            string temp = "", temp2 = "";
            int limiMayo = 0, limiMeno = 0, tempMayo = 0;
            for (int i = 0; i < numeros.Length; i++)
            {
                temp2 = numeros[i] + "";
                if ((i + 1) != numeros.Length)
                {
                    if (temp2.Equals(" "))
                    {
                        if (0 == limiMayo)
                        {
                            limiMeno = limiMayo;
                        }
                        else
                        {
                            limiMeno = limiMayo + 1;
                        }
                        limiMayo = i;
                        tempMayo = i - limiMeno;
                        temp = numeros.Substring(limiMeno, tempMayo);
                        MayoMeno(int.Parse(temp));
                    }
                }
                else
                {
                    limiMeno = limiMayo;
                    limiMayo = i;
                    tempMayo = i - limiMeno;
                    temp = numeros.Substring(limiMeno, tempMayo);
                    MayoMeno(int.Parse(temp));
                }
            }
            Console.WriteLine("EL numero mayor es: " + mayoNume + " y el menor es: " + menoNume);
            Console.ReadLine();
        }
        private static void MayoMeno(int numero)
        {
            if (numero > mayoNume)
            {
                mayoNume = numero;
            }
            else if (numero < menoNume)
            {
                menoNume = numero;
            }
        }
    }
}
