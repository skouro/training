; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; tokenize whitespace separated string
(defn stoi
  ([string]
    (map #(Integer/parseInt %)
      (clojure.string/split string #" ")
    )
  )
)

; add tokens
(defn process_line
  ([line]
    (let [v (stoi line)]
      (apply + v)
    )
  )
)

; parse file line by line
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (if-not (clojure.string/blank? line)
          (println (process_line line))
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
  )
)

; $lein run
; 18772
