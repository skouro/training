/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

// Unnecesary or conflicting rules
/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */
/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */

const nsteps = 20;
const scale = 100;
const beta = 0.8;
const mathjs = require('mathjs');

function createGraph(msize, edges) {
  const igraph = Array.from({ length: msize }, () =>
    Array.from({ length: msize }, () => 0));
  const congraph = igraph.map((row, rowi) => row.map((elmt, colj) =>
    edges.filter((eij) =>
      (eij[0] === rowi) && (eij[1] === colj)).length));
  const graph = congraph.map((row) => {
    const rsize = row.reduce((acc, val) => acc + val);
    return row.map((elmt) => elmt * beta / rsize);
  });
  const matz = Array.from({ length: msize }, () =>
    Array.from({ length: msize }, () => (1 - beta) / msize));
  return mathjs.add(graph, matz);
}

function runGraph(graph, steps, walkers) {
  if (steps === 0) {
    return walkers.map((val) => val * scale).map((val) => val.toFixed(2));
  }
  const updwalkers = mathjs.multiply(walkers, graph);
  return runGraph(graph, steps - 1, updwalkers);
}

function getPageRank(msize, edges) {
  const graph = createGraph(msize, edges);
  const walkers = Array.from({ length: msize }, () => 1 / msize);
  const ranks = runGraph(graph, nsteps, walkers);
  return ranks;
}

function findPageRank(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const [ msize ] = dataLines.slice(0, 1)[0].split(' ')
    .map(Number).slice(0, 1);
  const edges = dataLines.slice(1).map((line) => line.split(' ').map(Number));
  const pagerank = getPageRank(msize, edges);
  pagerank.map((val) => console.log(val));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findPageRank(readerr, contents));
}

main();

/*
$ node badwolf10.js
10.59 9.71 19.25 8.86 11.77 4.82 7.13 6.71 4.68 16.66
*/
