print_string "ingrese dos numeros separados por un espacio: \n";;
let data = (read_line ());;
let dataList = List.map int_of_string(Str.split (Str.regexp " ") data);;
let a = (List.nth dataList 0);;
let b = (List.nth dataList 1);;
let count = ref 0;;
let operations str =
  let cells = Array.make 4096 0 in
    let ptr = ref 0 in
    let len = String.length str in
    let rec evaluation c =
        if c >= len then
            c
        else            
            match str.[c] with
                | '>' -> ptr := !ptr + 1; evaluation (c+1)
                | '<' -> ptr := !ptr - 1; evaluation (c+1)
                | '+' -> cells.(!ptr) <- cells.(!ptr) + 1; evaluation (c+1)
                | '-' -> cells.(!ptr) <- cells.(!ptr) - 1; evaluation (c+1)                
                | ':' -> Printf.printf "%i \n" (Char.code (Char.chr cells.(!ptr))); evaluation (c+1)
                | '[' ->                    
                    let newp = ref c in
                    while cells.(!ptr) != 0 do
                        newp := evaluation (c+1)
                    done;
                    evaluation (!newp+1)
                | ']' -> c
                | ';' -> (
                            if (!count = 0)then(
                                count := !count + 1;
                                cells.(!ptr) <- (a); evaluation (c+1);
                            )else(
                                cells.(!ptr) <- (b); evaluation (c+1);
                            )    
                         )
                | _ -> failwith "simbolo no valido"
    in
    ignore (evaluation 0)
;;
operations ";>;<[>>>+>+<<<<-]>>>>[<<<<+>>>>-]<<<[>>>+>+<<<<-]>>>>[<<<<+>>>>-]<<<<<>[-<+>]<[>>+<<-]>>>[<<<+>>>-]>[<<<+>>>-]<<:";;
