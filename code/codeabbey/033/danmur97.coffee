###
$ coffeelint --reporter jslint danmur97.coffee #linting
$ coffee -c danmur97.coffee #compilation
###

main = (x)->
  data = x[2].split " "
  str = ""
  for i in [0...data.length]
    str = str + corruptedData_filter(parseInt(data[i]))
  console.log(str)
  0

corruptedData_filter = (x)->
  r = ""
  c = count_ones((x).toString(2))
  if(c % 2 == 0)
    r = String.fromCharCode(x & 127)
  r

count_ones = (x)->
  count = 0
  for i in [0...x.length]
    if(x.charAt(i)=="1")
      count++
  count

main(process.argv)

###
$ coffee danmur97.coffee "$(< DATA.lst)"
8cLWf1TcI Q9EF YtRdnWx  M3QwU6 d0fJp6Q klg4Khj rjcRjScf0u8VCqTZ.
###
