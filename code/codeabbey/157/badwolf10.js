/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression*/


/* some ranges are know to not have an emirp prime, we can dismiss those ranges
from searching to speed up search to realistic time, we define some anchor
points to narrow down the iteration range */

const anctwo = 20000000000000000000000;
const ancthree = 30000000000000000000000;
const ancfour = 40000000000000000000000;
const ancseven = 70000000000000000000000;
const anceight = 80000000000000000000000;
const ancnine = 90000000000000000000000;

const bigInt = require('big-integer');

const randomInt = require('random-int');
const maxrnd = 1010101;

function toBigInt(stnum) {
  return bigInt(stnum);
}

function narrowDownRanges(snum) {
  if (snum.greater(anctwo) && snum.lesser(ancthree)) {
    return bigInt(ancthree.toString());
  } else if (snum.greater(ancfour) && snum.lesser(ancseven)) {
    return bigInt(ancseven.toString());
  } else if (snum.greater(anceight) && snum.lesser(ancnine)) {
    return bigInt(ancnine.toString());
  }
  return snum;
}

function primalityTest(tnumber) {
  const rnum = bigInt(randomInt(0, maxrnd));
  if (rnum.modPow(tnumber.minus(1), tnumber).eq(1)) {
    return true;
  }
  return false;
}

function getEmirp(tnum) {
  const currnum = narrowDownRanges(tnum);
  if (primalityTest(currnum)) {
    const emirp = currnum.toString().split('').reduce((acc, bval) =>
      ([ bval, ...acc ])).reduce((acc, bval) => acc + bval);
    if (primalityTest(bigInt(emirp))) {
      return currnum;
    }
  }
  return getEmirp(currnum.plus(1));
}

function findEmirp(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const snums = dataLines.slice(1).map(toBigInt);
  const emirps = snums.map(getEmirp);
  // eslint-disable-next-line no-console
  emirps.map((emirp) => console.log(emirp.toString()));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findEmirp(readerr, contents));
}

main();

/*
$ node badwolf10.js
30939269335276755753029
70542276912119908564307
30542990501070864739151
30180029436013545780049
90668339652399947893099
90377870022727936643971
10554112870693480545563
90723707961999868564111
70000000000000000000859
90000000000000000000001
30000000000000000000047
70000000000000000000859
70891076823639578114099
70344452305513385070083
10782355894393623321557
70000000000000000000859
10132869121360915557049
30718898702116166538083
*/
