x = [cadena.split(' ') for cadena in 
"""7742297 3520775
6035 1208
7375 1998
2105134 23
15153 1848
-9951045 2537171
8804789 486
6445822 209
4486051 -4719015
4479226 -241925
-8673701 -1370776
8945525 -98863
14959 1522
17397 1718
15501 1234
5237 1238
-7708629 -1096351""".splitlines()]

for i in range(len(x)):
    cociente=int(x[i][0])/int(x[i][1])
    parte_entera=int(cociente)
    parte_decimal=abs(cociente)-abs(int(cociente))
    
    if parte_decimal>=0.5:
        if cociente>0:
            redondeo=parte_entera+1
        else:
            redondeo=parte_entera-1
    elif 0<=parte_decimal<0.5:
        redondeo=parte_entera
    print(redondeo, end=" ")






