"use strict";

/**
 * Rounds a fraction to the nearest integer.
 * @param {number} num The numerator of the fraction.
 * @param {number} den The denominator of the fraction.
 * @return {number} The fraction num/den rounded to the nearest integer.
*/
function roundFraction(num, den) {
    return Math.round(num / den);
}

// IO
var fs = require("fs");
var lines = fs.readFileSync("DATA.lst").toString().split("\n");
var salida = "";
var i;
var line;
for (i = 1; i < lines.length; i += 1) {
    line = lines[i].split(" ");
    salida += roundFraction(Number(line[0]), Number(line[1])) + " ";
}
console.log(salida);
