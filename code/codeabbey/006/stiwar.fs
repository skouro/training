open System

[<EntryPoint>]
let main argv =
    let Operations (myfrac: float) =
        let result = (myfrac- Math.Floor(myfrac))*10.0
        if (result = 0.0) then (int myfrac)
        elif (myfrac>0.0) then (
            if(result>=5.0) then (int (Math.Ceiling(myfrac)))
            else (Convert.ToInt32(myfrac))
            )
        elif(result>=5.0) then (int (Math.Ceiling(myfrac))  )
            else (Convert.ToInt32(myfrac))
    printfn "ingrese la cantidad de pruebas a realizar:"
    let c = stdin.ReadLine() |> int  // '|> int' cast a int
    let mutable output = Array.zeroCreate c
    for i in 0 .. (c-1) do
        printfn "(%i) ingrese dos numeros separados por un espacio" (i+1)
        let pairArray = stdin.ReadLine().Split ' ' //array de string
        let fra = (float pairArray.[0])/(float pairArray.[1])
        output.[i]<-Operations fra
    for i in 0 .. (c-1) do
     printf "%i " output.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
