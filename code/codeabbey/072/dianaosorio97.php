<?php
/**
 * Funny Words Generator
 *
 * PHP version 7.2.10
 * phpcs dianaosorio97.php
 *
 * @category Challenge
 * @package  Words
 * @author   Diana Osorio <dianaosorio66@gmail.com>
 * @license  GNU General Public License
 * @link     none
 */

if (file_exists('DATA.lst')) {
    $archivo = fopen("DATA.lst", "r") or die("No se pudo abrir el archivo");
    $array = file("DATA.lst");
    $datos = explode(" ", $array[0]);
    $n_palabras = explode(" ", $array[1]);
    $vocales = array('a','e','i','o', 'u');
    $cosonantes = array('b','c','d','f','g','h','j','k',
    'l','m','n','p','r','s','t','v','w','x','z');
    $a = 445;
    $c = 700001;
    $m = 2097152;
    $aux = 0;
    $result = 0;
    $palabra = " ";

    $aux =(int) $datos[1];

    for ($i=0; $i <$datos[0] ; $i++) {
        for ($j=0; $j < $n_palabras[$i]; $j++) {
            $aux = ($a*$aux+$c)%$m;
            if ($j%2 == 0) {
                $result = $aux%19;
                $palabra[$j] = $cosonantes[$result];

            } else {
                $result = $aux%5;
                $palabra[$j] = $vocales[$result];

            }

        }
        echo $palabra;
        echo " ";
        $palabra = " ";
    }


    echo "\n";
} else {
    echo "Fallo";
}


/*
php dianaosorio97.php
output:
hoj ladog xopim mipu direco sizucowo
cebagebe kezuv jep cocov misu lovasaf
pebolo fici teb vimok bejis dipanat
hasekad cunig kukipada hehaho
*/
?>
