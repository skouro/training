#!/bin/bash
#
# Problem #7 Fahrenheit to Celsius
#
while read -r line || [[ -n "$line" ]]; do
    FRACTION_PART=0.5
    data_array=($line)
    count=0
    i=0
    for item in "${data_array[@]}"
    do
        if [[ "$count" -gt 0 ]]
        then
            sum=$((item-32))
            conversion=$(echo "scale=1; $sum/1.8" | bc)
            compare=$(echo "$conversion>0" | bc)

            if [[ "$compare" -gt 0 ]]
            then
                conversion=$(echo "$conversion+$FRACTION_PART" | bc)
            else
                conversion=$(echo "$conversion-$FRACTION_PART" | bc)
            fi

            array_output[$i]="${conversion%.*}"
            let "i+=1"
        else
            let "count+=1"
        fi
    done
    printf '%s ' "${array_output[@]}"    
done < "$1"
