# $ rubocop jdanilo7.rb
# Inspecting 1 file

# 1 file inspected, no offenses detected

def read_file(filename)
  lines = []
  File.foreach(filename) do |line|
    lines.push(line.split(' '))
  end
  lines
end

def guesser(guesses)
  guessed = 0
  found = false
  until found
    padded = format('%04d', guessed)
    matched = count_matches(guesses, padded)
    found = true if matched == guesses.length
    guessed += 1
  end
  padded
end

def count_matches(guesses, padded)
  matches = 0
  guesses.each do |guess|
    matched = compare_guess(padded, guess)
    matches += 1 if matched
  end
  matches
end

def compare_guess(padded, guess)
  num_right = guess[1].to_i
  matches = if num_right > 0
              compare_guess_non_zero(padded, guess[0], num_right)
            else
              compare_guess_zero(padded, guess[0], num_right)
            end
  matches
end

def compare_guess_non_zero(padded, guess, num_right)
  i = 0
  matches = false
  while i < padded.length && num_right > 0
    num_right -= 1 if padded[i] == guess[i]
    matches = true if num_right.zero?
    i += 1
  end
  matches
end

def compare_guess_zero(padded, guess, num_right)
  i = 0
  matches = false
  while i < padded.length
    num_right += 1 if padded[i] != guess[i]
    matches = true if num_right == 4
    i += 1
  end
  matches
end

input = read_file('DATA.lst')
input.shift
puts guesser(input)

# $ ruby jdanilo7.rb
# 5347
