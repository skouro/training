'''
Problem #111 Necklace Count
$ pylint develalopez.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 develalopez.py
'''

from __future__ import print_function


def pow_finder(sect, sub):
    '''
    Returns a prime number of possible equal subsections of the necklace in
    order to apply Fermat's Little Theorem and return whole numbers.

    If the previous iteration involved a divisor of the beads quantity,
    it will be returned here.
    '''
    if sub == 0:
        return sect
    return pow_finder(sub, sect % sub)


def necklaces(colors, beads):
    '''
    Function that gets the number of different necklaces possible for a pair
    of data.
    '''
    ans_num = 0
    ans_den = beads
    for i in range(1, beads+1):
        ans_num += colors ** (pow_finder(i, beads))
    return int(ans_num/ans_den)


def main():
    '''
    Program that returns the number of necklaces that can be made of a given
    number of beads of different colors in a number of test cases, applying
    Fermat's Little Theorem.
    '''
    d_file = open("DATA.lst", "r")
    cases = int(d_file.readline())
    answers = []
    for _ in range(cases):
        colors, beads = map(int, [int(x) for x in d_file.readline().split()])
        answers.append(necklaces(colors, beads))
    print(" ".join(str(x) for x in answers))


main()

# pylint: disable=pointless-string-statement

'''
$ py develalopez.py
834 956635 8230 45 2195 1119796 11
'''
