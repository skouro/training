(ns suminloop.core
  (:gen-class))

(defn -main []
  (def numeros '(1085 1030 573 910 1273 543 724 1208 904 212 1120 1102 264 1277 733 1078 748 114 1052 836 1105 718 1159 563 385 93 192 687 70 168 1259 1145 1189 532 755 1161 1066 179 1068))
  (def answer (reduce + numeros))
  (println "La respuesta es " answer))
