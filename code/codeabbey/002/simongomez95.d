/*
$ dmd simongomez95.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;

void main(string[] args) {
  File file = File("DATA.lst", "r");
  string countstring = file.readln();
  countstring = stripws(countstring);
  int count = to!int(countstring);
  string[] numberstring = file.readln().split(" ");
  int sum = 0;
  for(int i=0; i<count; i++) {
    numberstring[i] = stripws(numberstring[i]);
    sum = sum+to!int(numberstring[i]);
  }
  file.close();
  writeln(sum);
}

string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*
$ ./simongomez95
31752
*/
