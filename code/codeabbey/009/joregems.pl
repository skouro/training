#$perl -Mstrict -Mdiagnostics -cw joregems.pl
#joregems.pl syntax OK
use List::Util qw[min max];
use List::Util qw/sum/;
use warnings;
use strict;
my $data = 'DATA.lst'; #the variable associated to the file that contains
#the triangle values
my $skipHeader=0; #variable for skipe the first line of the file
open(FH, '<', $data) or die $!; #open the file with the triangle values
while(<FH>){ #go through the file line by line
  my $totalLength=0; #the value of the sum of all sides of a triangle
  my @triangleArray; #the triangle in array version
  my @list = split(' ', $_); #spliting the split of the file to make it
  #manageable
  foreach my $value (@list){#go through triangleArray for make operations
    push @triangleArray, $value+0; # put the value in the array casting to int
      $totalLength= $totalLength+$value+0; #calculating totalLength
    }
    if($skipHeader>0){
      if ($totalLength-max(@triangleArray) >=max(@triangleArray)) {
      #the algorithm works with the fact that if the longest side is greater
      #or equal to the sum of the other two sides, then u can't make a triangle
        print "1\x20";
      }
      else{
        print "0\x20"
      }
    }
    $skipHeader++;
}
#$ perl joregems.pl.bck
#0 0 1 0 0 1 0 1 0 0 1 0 1 1 1 0 1 0 0 0 1 1
