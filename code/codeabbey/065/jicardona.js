#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

const [ input ] = arguments;
const lines = input.split(/\n/);

const edges = lines.shift();
const roads = [];
const nodes = [];

for (let edge = 0; edge < edges; edge++) {
  roads.push(lines.shift().split(' - '));
  nodes.push(roads[edge][0], roads[edge][1]);
}

const states = Array.from(new Set(nodes));

const dist = [];
for (let boop = 0; boop < states.length; boop++) {
  dist[boop] = [];
  for (let flob = 0; flob < states.length; flob++) {
    if (boop === flob) {
      dist[boop][flob] = 0;
    } else {
      dist[boop][flob] = 10000000;
    }
  }
}

for (const road of roads) {
  dist[states.indexOf(road[0])][states.indexOf(road[1])] = 1;
  dist[states.indexOf(road[1])][states.indexOf(road[0])] = 1;
}

for (let boop = 0; boop < states.length; boop++) {
  for (let flob = 0; flob < states.length; flob++) {
    for (let norf = 0; norf < states.length; norf++) {
      if (dist[boop][flob] > dist[boop][norf] + dist[norf][flob]) {
        dist[boop][flob] = dist[boop][norf] + dist[norf][flob];
      }
    }
  }
}

const tests = lines.shift();
const routes = [];

for (let test = 0; test < tests; test++) {
  const route = lines.shift().split(' - ');
  routes.push(dist[states.indexOf(route[0])][states.indexOf(route[1])]);
}

print(routes.join(' '));

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
2 2 3 2 2 2 3 2 3 2 2 3
*/
