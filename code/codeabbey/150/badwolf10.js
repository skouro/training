/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */

// some functions do not need to return a value
/* eslint-disable fp/no-unused-expression*/

const octregex = new RegExp('^0[0-7]+$');
const hexregex = new RegExp('^((0(x|X)[0-9A-Fa-f]+)|\\d[A-Za-z0-9]*(h|H))$');
const binregex = new RegExp('^(0(b|B)[0-1]+)$|^([0-1]+(b|B))$');
const decregex = new RegExp('^[1-9]\\d*$');

function findMatch(strnum) {
  if (octregex.test(strnum)) {
    return 'oct';
  } else if (hexregex.test(strnum)) {
    return 'hex';
  } else if (binregex.test(strnum)) {
    return 'bin';
  } else if (decregex.test(strnum)) {
    return 'dec';
  }
  return strnum;
}

function findType(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const typelist = dataLines.map(findMatch);
  // eslint-disable-next-line no-console
  typelist.map((x) => console.log(x));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findType(readerr, contents));
}

main();
/*
$ node badwolf10.js
oct dec dec bin 088 dec J2DH dec hex hex oct bin 0B21111210 bin hex hex oct
oct dec dec dec bin 0b110202001 031a1 dec oct hex hex 0xghf oct oct bin oct
bin bin hex
*/
