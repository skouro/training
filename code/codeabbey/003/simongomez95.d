/*
$ dub run dscanner -- -s simongomez95.d
Building package dscanner in /Users/nekothecat/.dub/packages/dscanner-0.5.11/ds
canner/
Running pre-generate commands for dscanner...
Performing "debug" build using /Library/D/dmd/bin/dmd for x86_64.
stdx-allocator 2.77.2: target for configuration "library" is up to date.
emsi_containers 0.8.0-alpha.9: target for configuration "unittest" is up to dat
e.
libdparse 0.9.8: target for configuration "library" is up to date.
dsymbol 0.4.8: target for configuration "library" is up to date.
inifiled 1.3.1: target for configuration "library-quiet" is up to date.
libddoc 0.4.0: target for configuration "lib" is up to date.
dscanner 0.5.11: building configuration "application"...
Linking...
To force a rebuild of up-to-date targets, run again with --force.
Running ../../../../../.dub/packages/dscanner-0.5.11/dscanner/bin/dscanner -s s
imongomez95.d

$ dub run dscanner -- -S simongomez95.d
Building package dscanner in /Users/nekothecat/.dub/packages/dscanner-0.5.11/ds
canner/
Running pre-generate commands for dscanner...
Performing "debug" build using /Library/D/dmd/bin/dmd for x86_64.
stdx-allocator 2.77.2: target for configuration "library" is up to date.
emsi_containers 0.8.0-alpha.9: target for configuration "unittest" is up to dat
e.
libdparse 0.9.8: target for configuration "library" is up to date.
dsymbol 0.4.8: target for configuration "library" is up to date.
inifiled 1.3.1: target for configuration "library-quiet" is up to date.
libddoc 0.4.0: target for configuration "lib" is up to date.
dscanner 0.5.11: building configuration "application"...
Linking...
To force a rebuild of up-to-date targets, run again with --force.
Running ../../../../../.dub/packages/dscanner-0.5.11/dscanner/bin/dscanner -S s
imongomez95.d

$ dmd simongomez95.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;

void main() {
  File file = File("DATA.lst", "r");
  string pairstring = stripws(file.readln());
  const int pairs = to!int(pairstring);
  string[] numberstring;
  int sum;
  string sums = "";
  for(int i=0; i<pairs; i++) {
    numberstring = file.readln().split(" ");
    sum = to!int(stripws(numberstring[0])) + to!int(stripws(numberstring[1]));
    sums = sums ~ " " ~ to!string(sum);
  }
  file.close();
  writeln(sums);
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*
$ ./simongomez95
602139 543855 1409727 707784 745831 738766 1493476 792403 881291 811859
732132 1458154 1016464 1525208 666583
*/
