;; $ lein eastwood
;;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;;   Directories scanned for source files:
;;     src test
;;   == Linting kamadoatfluid.clj ==
;;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

;; namespace
(ns kamadoatfluid.clj
  (:gen-class))

;; casts a string into a double
(defn todbl [s]
  (Double/parseDouble s))

;; casts a string into an int
(defn toint [s]
  (Integer/parseInt s))

;; get a float random number between [a, b)
(defn frand [a b]
  (+ a (rand (- b  a))))

;; get an int random number between [a, b)
(defn irand [a b]
  (+ a (rand-int (+ (- b  a) 1))))

;; square a number by long multiplication
(defn pow-2 [n]
  (* n n))

;; normalize a number 'n' into an interval [0.0 100.0]
(defn nlz [n]
  (min (max n 0.0) 100.0))

;; load lines of file handled by 'rdr' into a vector of double
(defn loadf [rdr]
  (vec (line-seq rdr)))

;; tokenize whitespace separated string into a vector of double
(defn get-tokens-vect [s]
  (vec (map #(todbl %) (clojure.string/split s #" "))))

;; returns the speed of the craft when reaching the planet surface
(defn get-reaching-speed [rocket-state procedure]
  ;; ==== Rocket Vars ====================
  ;;   V  = speed                   [ m/s]
  ;;   H  = altitude                [ m/s]
  ;;   Mc = mass of craft           [  kg]
  ;;   Mf = mass of remaining fuel  [  kg]
  ;;   Br = fuel burning rate       [kg/s]
  ;; =====================================
  (with-local-vars [Mc     (get rocket-state 0)
                    Mf     (get rocket-state 1)
                    H      (get rocket-state 2)
                    V      (get rocket-state 3)

                    ft     (get procedure 0)
                    Bro    (get procedure 1)
                    dBr    (get procedure 2)

                    mr     1737100.0
                    es     2800.0
                    go     1.622
                    dt     0.01

                    t      0
                    g      0.0
                    Br     0.0
                    dV     0.0
                    dMf    0.0]
    (while (> @H 0.0)
      (if (< @t @ft)
        (var-set Br 0.0)
        (if (> @Mf 0.0)
          (var-set Br (min (max (+ @Bro (* @dBr (- @t @ft))) 0.0) 100.0))
          (var-set Br 0.0)))
      (dotimes [i 100]
        (if (> @H 0.0)
          (do
            (var-set H (- @H (* @V @dt)))
            (if (> @Mf 0.0)
              (do
                (var-set dMf (* @Br @dt))
                (var-set dV  (/ (* @es @dMf) (+ @Mf @Mc)))
                (var-set Mf  (- @Mf @dMf)))
              (do
                (var-set dMf 0.0)
                (var-set Mf  0.0)
                (var-set dV  0.0)))
            (var-set g (/ (* @go (pow-2 @mr)) (pow-2 (+ @mr @H))))
            (var-set V (+ @V (- (* @g @dt) @dV))))))
      (var-set t (+ @t 1.0)))
    @V))

;; get optimal landing parameters for the given rocket state
(defn optimize-landing [rocket-state]
  (with-local-vars [i    0
                    a    (irand 30 90)
                    b    (irand 0 100)
                    c    0.0

                    da   0
                    db   0.0
                    dc   0.0

                    V    10000.0
                    nV   0.0

                    params (list 0.0)]
    (while (> @V 4.0)
      (var-set i    0)
      (var-set a    (irand 30 90))
      (var-set b    (irand 0 100))
      (var-set c    0.0)
      (var-set V    10000.0)
      (while (and (> @V 4.0) (< @i 1000))
        (var-set i (+ @i 1))
        (if (> @V 250.0)
          (do
            (var-set da (irand -1 1))
            (var-set db (irand -1 1))
            (var-set dc 0.0))
          (do
            (var-set da 0.0)
            (var-set db (frand -0.01 0.01))
            (var-set dc (frand -0.001 0.001))))

        (var-set params (vec (list (+ @a @da) (nlz (+ @b @db)) (+ @c @dc))))
        (var-set nV  (get-reaching-speed rocket-state @params))

        (while (< @nV @V)
          (do
            (var-set V @nV)
            (var-set a (+ @a @da))
            (var-set b (nlz (+ @b @db)))
            (var-set c (+ @c @dc))

            (var-set params (vec (list (+ @a @da) (nlz (+ @b @db)) (+ @c @dc))))
            (var-set nV     (get-reaching-speed rocket-state @params))))))
    (println @a @b @c "landing V: " @V)))

;; parse DATA.lst and solve
(defn process_file [file]
  (with-open [rdr (clojure.java.io/reader file)]
    (let [f       (loadf rdr)]
      (dotimes [i (toint (get f 0))]
        (let [rocket-state  (get-tokens-vect (get f (+ i 1)))]
          (optimize-landing rocket-state))))))

;; entry point
(defn -main [& args]
  (process_file "DATA.lst"))

;; $lein run
;; 32.0 56.968823618467056 -0.0028243064257257103 landing V:  2.440505204374588
;; 211.0 99.00046530336874 0.005670491367521934 landing V:  2.2022241014496653
;; 141.0 32.49356989447452 0.01026965265748409 landing V:  1.7752328223844733
;; 19.0 35.925558127824836 0.00755524355054327 landing V:  3.403949618084715
;; 68.0 37.58379956713647 0.00855258959884956 landing V:  1.1806669582664928
