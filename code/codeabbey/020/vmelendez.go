/*
$ go lint vmelendez.go
$ go run vmelendez.go
*/
package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
)

func main() {
  var x [100]string

  f, err := os.Open("DATA.lst")
  if err != nil {
    fmt.Printf("error opening file: %v\n", err)
    os.Exit(1)
  }

  scanner := bufio.NewScanner(f)

  i := 0
  for scanner.Scan() {
    x[i] = scanner.Text()
    i++
  }

  n, _ := strconv.Atoi(x[0])
  p := make([]int, n, 2*n)

  for i := 1; i < n+1; i++ {
    for j := 0; j < len(x[i]); j++ {
      if (x[i][j] == 97) || (x[i][j] == 101) || (x[i][j] == 105) ||
      (x[i][j] == 111) || (x[i][j] == 117) || (x[i][j] == 121) {
        p[i-1]++
      }
    }
  }

  for i := 0; i < n; i++ {
    fmt.Printf("%v ", p[i])
  }
}
/*
$ go run vmelendez.go
5 4 13 2
*/
