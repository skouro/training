#!/usr/bin/env python3
"""
Problem #12 Modulo and time difference
"""
class ModuloAndTimeDifference:

    output = []
    def __init__(self):

        while True:

            line = input()
            if line:
                if len(line) < 3:
                    pass
                else:
                    test_case = line.split()
                    date_in_seconds = self.date_to_seconds(test_case)
                    d1 = self.date_to_seconds(test_case[:4])
                    d2 = self.date_to_seconds(test_case[4:])
                    total = d2 - d1
                    self.seconds_to_date(total)
            else:
                break

        text = ' '.join(self.output)
        print(text)

    def date_to_seconds(self, date):

        result = int(date[0])*24*60*60+\
                int(date[1])*60*60+\
                int(date[2])*60+\
                int(date[3])

        return result

    def seconds_to_date(self, date):

        date = int(date)
        remainder = []
        count = 0
        while date > 0:

            if count < 2:
                remainder.append(date%60)
                count += 1
                date = date // 60
            else:
                remainder.append(date%24)
                count = 0
                date = date // 24

        if len(remainder) == 3:
            remainder.append(1)

        remainder = remainder[::-1]
        format_date = "({r[0]} {r[1]} {r[2]} {r[3]})".format(r=remainder)
        self.output.append(format_date)

ModuloAndTimeDifference()
