using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Número de repeticiones: ");
            String resultado = "";
            int repeticiones = int.Parse(Console.ReadLine());
            for (int i = 0; i < repeticiones; i++)
            {
                int dia, hora, min, seg;
                int dia1 = int.Parse(Console.ReadLine());
                int hor1 = int.Parse(Console.ReadLine());
                int mes1 = int.Parse(Console.ReadLine());
                int seg1 = int.Parse(Console.ReadLine());
                int dia2 = int.Parse(Console.ReadLine());
                int hor2 = int.Parse(Console.ReadLine());
                int mes2 = int.Parse(Console.ReadLine());
                int seg2 = int.Parse(Console.ReadLine());
                if (seg2 >= seg1)
                    seg = seg2 - seg1;
                else
                {
                    mes2--;
                    seg2 = seg2 + 60;
                    seg = seg2 - seg1;
                }
                if (mes2 >= mes1)
                    min = mes2 - mes1;
                else
                {
                    hor2--;
                    mes2 = mes2 + 60;
                    min = mes2 - mes1;
                }
                if (hor2 >= hor1)
                    hora = hor2 - hor1;
                else
                {
                    dia2--;
                    hor2 = hor2 + 24;
                    hora = hor2 - hor1;
                }
                dia = dia2 - dia1;
                Console.WriteLine("Otro ciclo");
                resultado = resultado + "(" + dia + " " + hora + " " + min + " " + seg + ") " + " ";
            }
            Console.WriteLine("resultado: " + resultado);
            Console.ReadKey();
        }
    }
}
