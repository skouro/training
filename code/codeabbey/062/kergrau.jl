#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#
using Primes

open("DATA.lst") do file
  flag = false

  for ln in eachline(file)
    index1 = 0
    index2 = 0
    if flag == false
      flag = true
      continue
    end
    i = split(ln, " ")
    index1 = parse(Int64, i[1])
    index2 = parse(Int64, i[2])
    n_prime = primes(index1, index2)
    println(length(n_prime))
  end
end

# $kergrau.jl
# 83240 8887 14177 7452 51121 4484 11804 31344 2119 60923 17235 27382 11202
# 38837 28516 43885 25869 5028 49326 9581 32738 3878 16131
# 535 54619 3369 5562
