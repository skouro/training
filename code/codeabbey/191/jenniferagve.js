/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function findMinMax(numIndex, compIndex, zeroDel, valMinMax, minimComp) {
/* Find the min or max value of the mass number and returns it*/
  if (compIndex === 0) {
    return valMinMax;
  }
  if ((zeroDel[compIndex] < valMinMax) && (minimComp === true)) {
    const valMinMaxNew = zeroDel[compIndex];
    return findMinMax(compIndex, compIndex - 1, zeroDel, valMinMaxNew,
      minimComp);
  } else if ((zeroDel[compIndex] > valMinMax) && (minimComp === false)) {
    const valMinMaxNew = zeroDel[compIndex];
    return findMinMax(compIndex, compIndex - 1, zeroDel, valMinMaxNew,
      minimComp);
  }
  return findMinMax(numIndex, compIndex - 1, zeroDel, valMinMax, minimComp);
}

function timesComp(times, convertion, zeroDel, minimComp) {
/* Comprobation for the first time of the seek thus 0 value
won't be in first position*/
  if (times === 0) {
    const minVal = findMinMax(0, zeroDel.length - 1, zeroDel, zeroDel[0],
      minimComp);
    if (minVal === zeroDel[0]) {
      const convertionN = convertion.slice(times + 1);
      return timesComp(times + 1, convertionN, zeroDel, minimComp);
    }
    return [ minVal, times ];
  }
  const minVal = findMinMax(0, convertion.length - 1, convertion,
    convertion[0], minimComp);
  if (minVal === convertion[0]) {
    const convertionN = convertion.slice(times + 1);
    return timesComp(times + 1, convertionN, zeroDel, minimComp);
  }
  return [ minVal, times ];
}

function minmaxSwap(minVal, minNumIndex, convertion, mostSig) {
/* Makes the swap of the min or max value with the original value*/
  const originalVal = convertion[mostSig];
  const hexBase = 16;
  if (minNumIndex === 0) {
    const hexRepSwap = convertion.map((element) => (element.toString(
      hexBase).toUpperCase()));
    const formatSwap = hexRepSwap.join('');
    return formatSwap;
  }
  const before = convertion.slice(0, mostSig);
  const firstPart = convertion.slice(mostSig + 1, minNumIndex);
  const secondPart = convertion.slice(minNumIndex + 1);
  const profitSwap = before.concat([ minVal ], firstPart, [ originalVal ],
    secondPart);
  const hexRepSwap = profitSwap.map((element) => (element.toString(
    hexBase).toUpperCase()));
  const formatSwap = hexRepSwap.join('');
  return formatSwap;
}

function dataManagement(element) {
/* process the data of the mass and calls the functions to make the changes
on the tax and wage final number mass*/
  const hexBase = 16;
  const zeroIndicator = 0;
  const oneIndicator = 1;
  const eachNum = element.split('');
  const convertion = eachNum.map((profitNumber) => parseInt(profitNumber,
    hexBase));
  const zeroDel = convertion.filter((firstCheck) => (firstCheck > 0));
  const minFix = timesComp(0, convertion, zeroDel, true);
  const minVal = minFix[zeroIndicator];
  const mostSig = minFix[oneIndicator];
  const minNumIndex = convertion.lastIndexOf(minVal);
  const taxSwap = minmaxSwap(minVal, minNumIndex, convertion, mostSig);
  const maxFix = timesComp(0, convertion, zeroDel, false);
  const maxVal = maxFix[zeroIndicator];
  const mostSigMax = maxFix[oneIndicator];
  const maxNumIndex = convertion.lastIndexOf(maxVal);
  const wagesSwap = minmaxSwap(maxVal, maxNumIndex, convertion, mostSigMax);
  const outputSwaps = process.stdout.write(`${ taxSwap } ${ wagesSwap } `);
  return [ outputSwaps ];
}

function weightFraud(erro, contents) {
/* Takes the content of the file and process each number of mass and gives
it to a function to make the  weight Fraud*/
  if (erro) {
    return erro;
  }
  const contSegment = contents.split('\n');
  const inputData = contSegment.slice(1);
  const finalValues = inputData.map((element) => dataManagement(element));
  return finalValues;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    weightFraud(erro, contents));
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();
/**
node jenniferagve.js
input:3
302A
10
31415926
-------------------------------------------------------------------------
output:203A A023 10 10 11435926 91415326
*/
