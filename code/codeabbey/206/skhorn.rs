/*
$ rustfmt skhorn.rs --write-mode=diff
$
$ rustc skhorn.rs
$
*/
//Read file
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("DATA.lst").unwrap();
    let mut padded_elements = Vec::new();
    let mut ascii_elements = Vec::new();
    let mut binary_elements = Vec::new();
    let mut concat_bin: String = Default::default();
    let mut chunk_array = Vec::new();
    let mut decimal_array = Vec::new();
    let mut ciphered = Vec::new();

    let mut index_elements = Vec::new();
    let mut bin_5b = Vec::new();
    let mut deciphered = Vec::new();
    let mut result = Vec::new();

    // odd lines normal text to be encoded
    // even lines encoded text to decoded
    let mut line_iter: i32 = 0;
    for line in BufReader::new(file).lines() {
        let current_line: &str = &line.unwrap();
        //println!("{}", current_line);
        if current_line.len() > 2 {
            line_iter += 1;
            //Checking if it is odd or even
            //println!("::::::::::::::::::::::::{}", line_iter);
            if (line_iter % 2) == 1 {
                // Encoding operation
                //println!("======== {} =======", "ENCODING BEGINS");

                // Padding elements
                padded_elements = padding(current_line);
                //println!("PADDING OPERATION == {:?}", padded_elements);

                // Convert symbol to ASCII
                ascii_elements = to_ascii(&padded_elements);
                //println!("ASCII DEC REPRESENTATION == {:?}", ascii_elements);

                // ASCII to Binary
                binary_elements = to_binary(&ascii_elements);
                //println!("ASCII TO BINARY == {:?}", binary_elements);

                // Concat all the array
                concat_bin = concat_all(&binary_elements);
                //println!("CONCAT == {}", concat_bin);

                // Split into 5 bytes chunks
                chunk_array = chunk_splitter(&concat_bin).to_vec();
                //println!("SPLITTING CHUNKS == {:?}", chunk_array);

                // Each bin chunk into its decimal repr
                decimal_array = to_decimal(&chunk_array);
                //println!("DECIMAL CHUNK == {:?}", decimal_array);

                // According to the alphabet given
                // Use decimal array as index an extract
                // its value
                ciphered = base32_encode(&decimal_array);
                //println!("{:?}", ciphered);

                let mut output: String = Default::default();
                for item in ciphered {
                    output += &item.to_string();
                }
                //println!("ENCODED RESULT ::: {}", output);
                result.push(output);

            //println!("======== {} =======", "ENCODING ENDS");
            } else {
                // Decoding operation
                //println!("======== {} =======", "DECODING BEGINS");

                index_elements = base32_decode(&current_line.to_string()).to_vec();
                //println!("DECODING OPERATION == {:?}", index_elements);

                bin_5b = to_binary_5b(&index_elements);
                //println!("BINARY REPR == {:?}", bin_5b);

                concat_bin = concat_all(&bin_5b);
                //println!("{}", concat_bin);

                // Split into 5 bytes chunks
                chunk_array = chunk_splitter_8b(&concat_bin);
                //println!("SPLITTING CHUNKS == {:?}", chunk_array);

                // Each bin chunk into its decimal repr
                decimal_array = to_decimal(&chunk_array);
                //println!("DECIMAL CHUNK == {:?}", decimal_array);

                deciphered = ascii_table(decimal_array);
                //println!("{:?}", deciphered);

                let mut fix_deci = remove_nums(deciphered);
                //println!("{}", fix_deci);
                result.push(fix_deci);
                //println!("======== {} =======", "DECODING ENDS");
            }
        } // if len end
    } // Bufreader end
    let mut concat_result: String = Default::default();
    for item in result {
        concat_result.push(' ');
        concat_result.push_str(&item);
    }
    println!("{}", concat_result);
} // Main end

fn padding(line: &str) -> std::vec::Vec<std::string::String> {
    let mut elements: Vec<&str> = line.split(' ').collect();
    let mut chain_elements: String = Default::default();
    for (i, item) in elements.iter().enumerate() {
        chain_elements += &item.to_string();
    }
    //println!("ITEM = {} LEN = {}", chain_elements, chain_elements.len());
  /*
  Line length needs to be multiple of five and the last item its padded
  with the difference of the multiple of five number and the length
  e.g if line length is 12, the closer multiple of five above it is 15
  15 - 12 = 3, where 3 is the quantity of padding items and at the same
  time the symbol to fill it.
  To calculate the whole length of the line, we take the len of the
  amount of items after the splitted and reduce it by 1, this operation
  will give the amount of whitespaces, which are accounted too on
  line length
  */
    let mut line_len: i32 = (chain_elements.len() as i32) + ((elements.len() as i32) - 1);
    let mut fix_len: i32 = Default::default();
    fix_len = multiple_of_five(line_len);
    //println!("LINE LEN :: {} FIX LEN :: {}", line_len, fix_len);
    // padding seq
    let mut fill_chain: String = Default::default();
    // padding symbol
    let mut int_flag: i32 = fix_len - (line_len);
    if line_len == fix_len {
        int_flag = 5;
        fix_len += 5;
    }
    // Generating the padding sequence
    while line_len < fix_len {
        fill_chain += &int_flag.to_string();
        line_len += 1;
    }
    //println!("FILL CHAIN{}", fill_chain);
    //println!("{}", elements[(elements.len() as usize -1)]);
    // Getting last element
    let mut last_element = elements.pop().unwrap();
    // Concat last element with padding sequence
    let mut fix_last = format!("{}{}", last_element, fill_chain);
    let mut padded_elements = Vec::new();
    for (i, item) in elements.iter().enumerate() {
        padded_elements.push(item.to_string());
    }

    padded_elements.push(fix_last);
    //println!("{:?}", padded_elements);
    padded_elements
}

// We gotta check if the length of whatever string
// comes is a multiple of 5, if not, fix it
// But, no string will overcome more than 20 characters
// And there is no need to do mod(%) operation
fn multiple_of_five(item_len: i32) -> i32 {
    let mut fix_len: i32 = Default::default();
    if item_len <= 5 {
        fix_len = 5;
    } else if item_len > 5 && item_len <= 10 {
        fix_len = 10;
    } else if item_len > 10 && item_len <= 15 {
        fix_len = 15;
    } else if item_len > 15 && item_len <= 20 {
        fix_len = 20;
    } else if item_len > 20 && item_len <= 25 {
        fix_len = 25;
    } else if item_len > 25 && item_len <= 30 {
        fix_len = 30;
    } else if item_len > 30 && item_len <= 35 {
        fix_len = 35;
    } else if item_len > 35 && item_len <= 40 {
        fix_len = 40;
    } else if item_len > 40 && item_len <= 45 {
        fix_len = 45;
    } else if item_len > 45 && item_len <= 50 {
        fix_len = 50;
    } else if item_len > 50 && item_len <= 55 {
        fix_len = 55;
    } else if item_len > 55 && item_len <= 60 {
        fix_len = 60;
    } else if item_len > 60 && item_len <= 65 {
        fix_len = 65;
    }
    fix_len
}

fn to_ascii(padded_elements: &[std::string::String]) -> std::vec::Vec<std::string::String> {
    let mut ascii_array = Vec::new();
    //println!("TO-ASCII {:?} LEN {}", padded_elements, padded_elements.len());
    let mut whitespace_count: i32 = 1;
    // Iterate over each element
    for (i, item) in padded_elements.iter().enumerate() {
        //println!("{}", item);
        // Iterate over each character then parse it to u32
        // Which will cast it to unicode integer
        for ch in item.chars() {
            let mut ascii = format!("{}", ch as u32);
            ascii_array.push(ascii);
        }
        if whitespace_count < (padded_elements.len() as i32) {
            ascii_array.push(32.to_string());
            whitespace_count += 1;
        }
    }
    //println!("{:?}", ascii_array);
    ascii_array
}

fn to_binary(ascii_elements: &[std::string::String]) -> std::vec::Vec<std::string::String> {
    let mut binary_array = Vec::new();
    // println!("{:?}", ascii_elements);
    for (i, item) in ascii_elements.iter().enumerate() {
        // println!("{}", item);
        // Parsing <string> to <u32>
        let s = item.parse::<u32>().unwrap();
        // Formatting number to its binary representation
        let bin = format!("{:08b}", s);
        binary_array.push(bin);
    }
    //println!("{:?}", binary_array);
    binary_array
}

fn concat_all(binary_elements: &[std::string::String]) -> String {
    let mut bin_concat: String = Default::default();
    // println!("{:?}", binary_elements);
    // Concatenating
    for (i, item) in binary_elements.iter().enumerate() {
        bin_concat = bin_concat + item;
    }
    //println!("{}", bin_concat);
    bin_concat
}

fn chunk_splitter(concat_bin: &str) -> std::vec::Vec<std::string::String> {
    //println!("{}", concat_bin);
    let mut new_chunk: String = Default::default();
    let mut chunk_array = Vec::new();
    let mut i: u32 = 1;
    // Iterating over each character
    for ch in concat_bin.chars() {
        new_chunk += &ch.to_string();
        // Whenever i is a multiple of five
        // that means its time to crop the chunk
        // And store it
        if (i % 5) == 0 && i != 0 {
            chunk_array.push(new_chunk);
            new_chunk = Default::default();
        }
        i += 1;
    }
    //println!("{:?}", chunk_array);
    chunk_array
}

fn to_decimal(chunk_array: &[std::string::String]) -> std::vec::Vec<i32> {
    //println!("{:?}", chunk_array);
    let mut decimal_array = Vec::new();
    // Iterate over array to get each chunk
    for (i, item) in chunk_array.iter().enumerate() {
        // Then, apply radix to get the decimal value
        let fmt = i32::from_str_radix(item, 2).unwrap();
        //println!("{}", fmt);
        decimal_array.push(fmt);
    }
    //println!("{:?}", decimal_array);
    decimal_array
}

fn base32_encode(decimal_array: &[i32]) -> std::vec::Vec<std::string::String> {
    let mut ciphered = Vec::new();
    // Given alphabet
    let alphabet = vec![
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z", "2", "3", "4", "5", "6", "7",
    ];
    // Iterate over decimal array
    for (i, item) in decimal_array.iter().enumerate() {
        // Cast i32 to usize
        let index = *item as usize;
        //println!("{}", alphabet[index].to_string());
        // Get whatever value is in that index
        ciphered.push(alphabet[index].to_string());
    }
    //println!("{:?}", ciphered);
    ciphered
}

fn base32_decode(ciphered: &str) -> std::vec::Vec<i32> {
    let alphabet = vec![
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z", "2", "3", "4", "5", "6", "7",
    ];
    //println!("{}", ciphered);
    let mut index_elements = Vec::new();
    for ch in ciphered.chars() {
        for (i, item) in alphabet.iter().enumerate() {
            //println!("{} == {}",ch, item );
            if ch.to_string() == item.to_string() {
                index_elements.push(i as i32);
            }
        }
    }
    index_elements
}

fn to_binary_5b(ascii_elements: &[i32]) -> std::vec::Vec<std::string::String> {
    let mut bin_elements = Vec::new();
    for (i, item) in ascii_elements.iter().enumerate() {
        //println!("{}", item);
        let mut fmt = format!("{:05b}", item);
        bin_elements.push(fmt);
    }
    bin_elements
}

fn chunk_splitter_8b(concat_bin: &str) -> std::vec::Vec<std::string::String> {
    //println!("{}", concat_bin);
    let mut new_chunk: String = Default::default();
    let mut chunk_array = Vec::new();
    let mut i: u32 = 0;
    // Iterating over each character
    for ch in concat_bin.chars() {
        if i < 8 {
            new_chunk += &ch.to_string();
        } else if i == 8 {
            i = 0;
            chunk_array.push(new_chunk);
            new_chunk = Default::default();
            new_chunk += &ch.to_string();
        }
        i += 1;
    }
    chunk_array.push(new_chunk);
    //println!("{:?}", chunk_array);
    chunk_array
}

fn ascii_table(decimal_array: std::vec::Vec<i32>) -> std::vec::Vec<std::string::String> {
    let mut decoded_str = Vec::new();
    let ascii_chars = vec![
        "NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL", "BS", "HT", "LF", "VT", "FF", "CR",
        "SO", "SI", "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB", "CAN", "EM", "SUB",
        "ESC", "FS", "GS", "RS", "US", " ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+",
        ",", "-", ".", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=",
        ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
        "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "'", "]", "^", "_", "`", "a",
        "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
        "t", "u", "v", "w", "x", "y", "z", "{", "|", "}", "~", "DEL",
    ];
    //println!("{:?}", decimal_array);
    for item in decimal_array {
        //println!("{}", ascii_chars[item as usize]);
        decoded_str.push(ascii_chars[item as usize].to_string());
    }
    decoded_str
}

fn remove_nums(deciphered: std::vec::Vec<std::string::String>) -> String {
    //println!("{:?}", deciphered);
    let mut fix_str: String = Default::default();
    for item in deciphered {
        if item != "1" && item != "2" && item != "3" && item != "4" && item != "5" {
            fix_str += &item;
        }
    }
    //println!("{}", fix_str);
    fix_str
}

/*
$ ./skhorn
NBQW423ZEBRXK4TSPFRW63LCMVSDGMZT roiled unformed overtaxes
MRXXOZLMEBUG65LOMRUW4ZZR lyricist squiggliest ORUHE2LGORUWK43UGU2TKNJV
amateurism radiotelephones blowzy graveling NFWWCZ3JNZTSA4DJNZTGKYLUNBS
XEMRS illustrator appendixes disengagements unobtrusively foolishly
O5QXMZLMMVXGO5DIOMQG22LTMVZGY2LOMVZXGIDUN52WO2DFON2DGMZT audio
MNQW433QPEQHA33TOR2WYYLUMVSCAYLNNZSXG5DZNFXGOIDHOJXWC3TTGU2TKNJV
eruditely ORZGS4DPMQQGS3TUMVXHG2LGPFUW4ZZAONUWIZLXMF4XGIDSMVSGK5TFNRXXAID
LNZQXA43BMNVTGMZT skated supplement sleazy NRXWOIDROVQXG2BR miscellany
unpopular overheard terrified NZXW43TFM5XXI2LBMJWGKIDJNVYG64TUMF2GS33OO
MQGM5LTNFRGYZJR lament adolescent teaspoonfuls cistern MRZGS3TLOM2DINBU
flowerpots computing ONUWO3TJMZUWGYLOOQ2DINBU successive eke intermarry
potholders churchman ORUG65LHNB2HGIDQN5WHS3LFOJUWGMRS
*/
