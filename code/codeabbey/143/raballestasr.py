#!/usr/bin/python3
""" This script performs the extended Euclidean algorithm
on a number of test cases and prints the gcd and coefficients.
"""

def extended_euclid(num1, num2):
    """ Performs the extended Euclidean algorithm on the given
    numbers and returns a list [g, a, b] such that
    a*num1 + b*num2 = g = gcd(num1, num2).
    """
    s_cur, t_cur, s_pre, t_pre = 0, 1, 1, 0
    r_cur, r_pre = num2, num1
    while r_cur:
        quo = r_pre // r_cur
        r_pre, r_cur = r_cur, r_pre - quo*r_cur
        s_pre, s_cur = s_cur, s_pre - quo*s_cur
        t_pre, t_cur = t_cur, t_pre - quo*t_cur
    print(str(r_pre) + " " + str(s_pre) + " " + str(t_pre))

F = open("DATA.lst", "r")
L = int(F.readline())
for l in range(L):
    num1_t, num2_t = list(map(int, F.readline().split()))
    extended_euclid(num1_t, num2_t)
F.close()
#
# pylint raballestasr.py 
# No config file found, using default configuration
# ************* Module raballestasr
# C: 25, 0: Final newline missing (missing-final-newline)
#
# --------------------------------------------------------------------
# Your code has been rated at 9.33/10 (previous run: -0.67/10, +10.00)
