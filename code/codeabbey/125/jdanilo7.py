# $ pylint jdanilo7.py #linting
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""
Bogosort based list sorter
"""


def random_gen(prev_x):
    """
    Generates Neumann random numbers
    """
    fst_three = int((prev_x - (prev_x % 1000))/1000)
    snd_three = int((prev_x % 1000))
    next_y = snd_three * 1000 + fst_three
    x_y = prev_x * next_y
    return int(((x_y % 1000000000) - (x_y % 1000))/1000)


def shuffle(rand, lst, length):
    """
    Shuffles a list using Neumann random numbers
    """
    for i in range(length):
        rand = random_gen(rand)
        j = rand % length
        lst[i], lst[j] = lst[j], lst[i]
    return lst, rand


def bogosort(seed, lst):
    """
    Sorts a list using bogosort
    """
    is_sorted = False
    shuffles = 0
    fail_lst = [i for i in lst]
    fail_seed = seed
    while not is_sorted:
        lst, seed = shuffle(seed, lst, len(lst))
        is_sorted = all(lst[i] <= lst[i+1] for i in range(len(lst)-1))
        shuffles += 1
        if (lst == fail_lst and seed == fail_seed):
            return -1
    return shuffles


def process_input():
    """
    Returns the number of shuffles required to order a list or -1
    if it is not possible to do so with the current bogosort set up
    """
    lines = open("DATA.lst", "r").read().splitlines()
    lines.pop(0)
    output = ""
    for line in lines:
        line = line.split(" ")
        lst = [int(num) for num in line]
        seed = 918255
        shuffles = bogosort(seed, lst)
        output += str(shuffles) + ' '
    return output


print(process_input())  # pylint: disable=superfluous-parens


# $ python jdanilo7.py
# -1 -1 -1 -1 2305 3619 -1 2054 -1 -1 -1 1154 3313
