# $ruby-lint kergrau.rb

answer = ''
player1 = 0
player2 = 0
flag = false

# open file code and itaration I take of
File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    unless flag
      flag = true
      next
    end
    line.split(' ').each do |game|
      case game
      when 'RS', 'SP', 'PR'
        player1 += 1
      when 'RP', 'SR', 'PS'
        player2 += 1
      else
      end
    end
    if player1 > player2
      answer << '1 '
    else
      answer << '2 '
    end

    player1 = 0
    player2 = 0
  end
end
puts answer

# ruby kergrau.rb
# 1 2 1 2 1 2 2 1 2 2 2 1 2 1 1 1 1 1 2 2 2
