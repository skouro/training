<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists('./DATA.lst')) {

  $file = fopen("./DATA.lst", "r");
  $c = fgets($file, 128);

  for($i = 0 ; $i < $c ; $i++) {

    $R = explode(' ',fgets($file,128));
    $P1 = $P2 = 0;

    foreach($R as $A) {

      if ((trim($A) == "RS")||(trim($A) == "PR")||(trim($A) == "SP"))
        $P1++;

      elseif ((trim($A) == "SR")||(trim($A) == "RP")||(trim($A) == "PS"))
        $P2++;
    }
    echo ($P1>$P2?1:2).' ';
  }
  fclose($file);
}

else
  echo "Error 'DATA.lst' not found";
/*
./ciberstein.php
1 2 1 2 1 2 2 1 2 2 2 1 2 1 1 1 1 1 2 2 2
*/
?>
