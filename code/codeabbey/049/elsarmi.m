clc
clear all
close all

% Rock-Paper-Scissors
%Rules:
%Rock beats Scissors (by blunting them)
%Scissors beat Paper (by cutting it)
%Paper beats Rock (by covering it)

series=input('Type how many series needed to evaluated: ','s');
x_games = str2double(series);
k = 0;
while k < x_games

    games = input('Enter the series of match/es for the game and divided by (;) each match with only allow two actions and has to be in Capital Letter: ','s');
    series = regexp(games,';','split');
    n = size(series,2);

    P1 = 0;
    P2 = 0;
    for i = 1 : n
        v = series{i};
        switch v 
            case 'RS'
                %win P1
                P1 = P1 + 1;
                disp('P1 game')
            case 'PR'
                %win P1
                P1 = P1 + 1;
                disp('P1 game')
            case 'SP'
                %win P1
                P1 = P1 + 1;
                disp('P1 game')
            case 'SR'
                %win P2
                P2 = P2 + 1;
                disp('P2 game')
            case 'RP'
                %win P2
                P2 = P2 + 1;
                disp('P2 game')
            case 'PS'
                %win P2
                P2 = P2 + 1;
                disp('P2 game')
            case 'SS'
                disp('Draw game')
            case 'PP'
                disp('Draw game')
            case 'RR'
                disp('Draw game')
            otherwise
                disp('Not valid series for unknown play')
        end
    end

    if P1 > P2
        disp('1: Player 1 wins the Serie')
    elseif P1 < P2
        disp('2: Player 2 wins the Serie')
    else
        disp('Draw serie')
    end
end  
