/*
$ rustfmt rafa1894.rs #linting
$ rustc rafa1894.rs #compilation
*/

use std::env;
use std::fs;

fn comp(s: &str) -> i32 {
    let mut count: i32 = 0;
    if s == "PR" || s == "RS" || s == "SP" {
        count += 1;
    } else if s == "PS" || s == "SR" || s == "RP" {
        count -= 1;
    }
    return count;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file = &args[1];
    let inputstr = fs::read_to_string(file).expect("Error");
    let mut x = 0;
    let mut y = 0;
    let lines = inputstr.lines();
    for i in lines {
        if x == 0 {
            x = 1;
            continue;
        }
        let game = i.split(' ');
        for round in game {
            y = y + comp(round);
        }
        if y > 0 {
            print!("1 ");
        } else {
            print!("2 ");
        }
        y = 0;
    }
}

/*
$ ./rafa1894 DATA.lst
1 2 1 2 1 2 2 1 2 2 2 1 2 1 1 1 1 1 2 2 2
*/
