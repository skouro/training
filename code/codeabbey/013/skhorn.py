#!/usr/bin/env python3
"""
Problem #13 Weighted sum of digits
"""
class WeightedSumOfDigits:

    result = []
    def __init__(self):

        while True:
            line = input()

            if line:
                if len(line) < 3:
                    pass
                else:
                    test_cases = line.split()
                    self.digits(test_cases)
            else:
                break

        text = ' '.join(self.result)
        print(text)

    def digits(self, cases):

        for counter, item in enumerate(cases):
            division = int(item)
            remainder = 0
            count = len(item)

            while division > 0:
                remainder += (division%10)*count
                division = division // 10
                count -= 1

            self.result.append(str(remainder))

WeightedSumOfDigits()
