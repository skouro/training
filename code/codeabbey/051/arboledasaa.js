/*
$ eslint jarboleda.js
$
*/
/*
code from
https://derickbailey.com/2014/09/21/
calculating-standard-deviation-with-array-map-and-array-reduce-in-javascript/
as in production a library would be used, lets not reinvent the wheel
also this is common public knowledge
*/

function average(values) {
  const summation = values.reduce((sum, value) =>
    (sum + value)
  , 0);
  const avg = summation / values.length;
  return avg;
}
/*
end of code from outside
*/

function numerationGenerator(currentArray, index, maxValue) {
  if (index >= maxValue) {
    return currentArray;
  }
  return numerationGenerator(currentArray.concat(index), index + 1, maxValue);
}
const maxNumberOfDices = 6;
const numberOfDices = numerationGenerator([], 1, maxNumberOfDices);
const numberOfSides = numerationGenerator([], 1, maxNumberOfDices + 1)
  .map((value) => (2 * value));
const adjustment = 0.5;
const avgTable = numberOfSides.map((side) => numberOfDices.map((value) =>
  (((side / 2) + adjustment) * value)));

const aHugeNumber = 1000;

function reducer(accumulator, currentValue) {
  return Math.min(accumulator, currentValue);
}

function solveForLine(numbers) {
  const avg = average(numbers.map((element) => parseInt(element, 10)));
  const minimumNumberInRange = Math.min(...numbers);
  const maximumNumberInRange = Math.max(...numbers);

  const distances = numberOfSides.map((currentValueOfI, indexI) => (
    numberOfDices.map((currentValueOfJ, indexJ) => {
      if ((minimumNumberInRange >= (currentValueOfJ)) &&
        (maximumNumberInRange <= (currentValueOfJ * currentValueOfI))) {
        return Math.abs(avg - avgTable[indexI][indexJ]);
      }
      return aHugeNumber;
    })));
  const reducedDistances = distances.map((row) =>
    (row.reduce(reducer))).reduce(reducer);
  const indexer = distances.map((row) => (row.indexOf(reducedDistances)));
  const theRow = indexer.map((cell) => (cell >= 0)).indexOf(true);
  const theRowValues = distances[theRow];
  const theColumn = theRowValues.indexOf(reducedDistances);
  const position = `${ numberOfDices[theColumn] }d${ numberOfSides[theRow] }`;
  return position;
}


function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const numberOfLines = 3;
  const splited = contents.split('\n').map((line) => (line.split(' ')
    .filter((value) => (value > 0)))).slice(0, numberOfLines);
  const answer = splited.map((line) => (solveForLine(line)))
    .join(' ');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
2d8 5d6 2d6

*/
