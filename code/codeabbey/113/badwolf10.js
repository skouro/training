/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */

/* This problem can be solved as an optimization problem:
- Let the N the number of sensors, each with with data [xk, yk, tk]
- Let dk be the distance from groundzero point (gz = [x, y, t]) to sensor k
- Let vk = dk/(t-tk) the propagation velocity from gz towards sensor k

We define a system of functions:
sqrt((x-x0)^2 + (y-y0)^2) - v(t-t0) = 0
sqrt((x-x1)^2 + (y-y1)^2) - v(t-t1) = 0
sqrt((x-x2)^2 + (y-y2)^2) - v(t-t2) = 0
sqrt((x-x3)^2 + (y-y3)^2) - v(t-t3) = 0

also written as
F(X) = 0

The system can be linearized and solved with Newton-Rapson method by the
iteration:

Xk+1 = Xk - [F'(Xk)]^(-1) * F(Xk)
*/

const rnd = require('random-int');
const ivel = 1000;
// const fsize = 10000;
const step = 1;
const minerror = 0.1;
const msize = 4;

function getDistance(sensorpoint, gztestpoint) {
/* Velocity for each trajectory from ground zero to each sensor Sk is:
   dk = sqrt((x-xk)^2 + (y-yk)^2)
*/
  const distance = Math.sqrt((Math.pow(gztestpoint[0] - sensorpoint[0], 2)) +
    (Math.pow((gztestpoint[1] - sensorpoint[1]), 2)));
  return distance;
}

function getDerivativeXorY(distances, sdataxyt, gzxyvt) {
/* Compute partial derivative with respect to X or Y distances has each di.
sdataxyt -> [[xi or yi, ti] ...] for each sensor measurement.
gzxyvt -> [x or y, v, t]. the current ground zero value.
Wwe use same method for X and Y partial derivatives.
*/
  const dxy = distances.map((dst, indx) =>
    (gzxyvt[0] - sdataxyt[indx][0]) / dst);
  return dxy;
}

function getDerivativeV(distances, sdatat, gzvt) {
/*
sdatavt -> [[Ti]...] sensors time measurements
gzvt -> [V,T] current ground zero values
The derivative for each component of Cost function is:
*/
  const dcv = distances.map((dst, indx) => gzvt[1] - sdatat[indx]);
  return dcv;
}

function getDerivativeT(distances, sdatat, gzvt) {
/*
sdatavt -> [[ti]...] sensors time measurements
gzvt -> [v,t] current ground zero values
The derivative for each component of Cost function is:
*/
  const dct = distances.map(() => gzvt[0]);
  return dct;
}

function computeGradient(sensordata, groundzero) {
  const distances = sensordata.map((smeasure) =>
    getDistance(smeasure, groundzero));
  const dxcomp = getDerivativeXorY(distances, sensordata.map((smd) =>
    [ smd[0], smd[2] ]), [ groundzero[0], groundzero[2], groundzero[2 + 1] ]);
  const dycomp = getDerivativeXorY(distances, sensordata.map((smd) =>
    [ smd[1], smd[2] ]), [ groundzero[1], groundzero[2], groundzero[2 + 1] ]);
  const dvcomp = getDerivativeV(distances, sensordata.map((smd) =>
    smd[2]), [ groundzero[2], groundzero[2 + 1] ]);
  const dtcomp = getDerivativeT(distances, sensordata.map((smd) =>
    smd[2]), [ groundzero[2], groundzero[2 + 1] ]);
  return [ dxcomp, dycomp, dvcomp, dtcomp ];
}

function computeCostFunction(sensordata, groundzero) {
  const distances = sensordata.map((smeasure) =>
    getDistance(smeasure, groundzero));
  const costi = sensordata.map((sdata, indx) =>
    distances[indx] - (groundzero[2] * (sdata[2] - groundzero[2 + 1])));
  return costi.map((csti) => Math.pow(csti, 2)).reduce((acc, val) => acc + val);
}

function computeFunction(sensordata, groundzero) {
  const distances = sensordata.map((smeasure) =>
    getDistance(smeasure, groundzero));
  const feqt = distances.map((dst, indx) =>
    dst - (groundzero[2] * (sensordata[indx][2] - groundzero[1 + 1 + 1])));
  return feqt;
}

function makeFisrtGuess(sensordata) {
/* first guess sometimes fails, center of mass of sensors or random coordinates
are used as methods of initialization*/
  const times = sensordata.map((sdata) => sdata[2]);
  const mtime = times.reduce((prev, curr) => {
    const val = prev < curr ? prev : curr;
    return val;
  });
  const xval = sensordata.map((sdata) => sdata[0]).reduce((acc, val) =>
    acc + val) / msize;
  const yval = sensordata.map((sdata) => sdata[0]).reduce((acc, val) =>
    acc + val) / msize;
  return [ xval, yval, rnd(ivel), rnd(Math.round(mtime)) ];
  // return [ rnd(fsize), rnd(fsize), rnd(ivel), rnd(Math.round(mtime)) ];
}

const tco = require('tco');
const mathjs = require('mathjs');

const findNewton = tco((sensordata, groundzero, cost, istep) => {
  if (cost < minerror) {
    // eslint-disable-next-line fp/no-nil
    return [ null, groundzero ];
  }
  // Compute linearization matrix F'(Xk)
  const gradientmatrix = computeGradient(sensordata, groundzero);
  // compute delta for update [F'(Xk)]^-1 * F(Xk)
  const finv = mathjs.inv(mathjs.transpose(gradientmatrix));
  const delta = mathjs.multiply(finv, computeFunction(sensordata, groundzero));
  // update Xk+1 = Xk - [GZ'(Xk)]^(-1) * GZ(Xk)
  const ugroundzero = groundzero.map((xyvtval, xyvt) =>
    xyvtval - (step * delta[xyvt]));
  const ucost = computeCostFunction(sensordata, ugroundzero);
  return [ findNewton, [ sensordata, ugroundzero, ucost, istep ] ];
});

function getGroundZero(sensordata) {
  return findNewton(sensordata, makeFisrtGuess(sensordata), 1);
}

function findGroundZero(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const sensordata = dataLines.filter((line) => line.length > 1)
    .map((line) => line.split(';')
      .filter((sdata) => sdata.length > 1)
      .map((triplets) => triplets.trim().split(' ').map(Number)))
    .map((sdata) => sdata.slice(0, msize));

  const groundzero = sensordata.map(getGroundZero);
  // eslint-disable no-console
  groundzero.map((gZero) =>
    console.log(`${ gZero[0].toFixed(0) } ${ gZero[1]
      .toFixed(0) } ${ gZero[1 + 1 + 1].toFixed(1) }`));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findGroundZero(readerr, contents));
}

main();

/*
$ node badwolf10.js
1319 5468 67.1
3839 7135 22.0
5348 5789 95.1
*/
