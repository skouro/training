/*
 * $ cppcheck kamadorueda.c
 *   Checking kamadorueda.c ...
 * $ splint kamadorueda.c
 *   Splint 3.1.2 --- 20 Feb 2018
 *   Finished checking --- no warnings
 * $
 */

#ifndef S_SPLINT_S
#include <stdio.h>
#include <math.h>

static inline void move_pos(double * x, double * y, char movement) {
  /*
  *  C B
  * D X A
  *  E F
  *
  */

  switch (movement) {
    case 'A':
      *x += 1.00000000000;
      break;
    case 'B':
      *x += 0.50000000000;
      *y += 0.86602540378;
      break;
    case 'C':
      *x -= 0.50000000000;
      *y += 0.86602540378;
      break;
    case 'D':
      *x -= 1.00000000000;
      break;
    case 'E':
      *x -= 0.50000000000;
      *y -= 0.86602540378;
      break;
    case 'F':
      *x += 0.50000000000;
      *y -= 0.86602540378;
      break;
  }
}

static inline void move_seq(double * x, double * y, char * movements) {
  char * movement = movements;
  do move_pos(x, y, *movement);
  while (*(++movement) != '\0');
}

int main() {
  unsigned int t;
  (void)scanf("%u", &t);
  for (; t != 0u; --t) {
    char movements[256];
    (void)scanf("%s", movements);

    // starts at x=0.0, y=0.0
    double x, y;
    x = 0.0;
    y = 0.0;

    // move a sequence of steps
    move_seq(&x, &y, movements);

    // print the distance from the starting point
    printf("%.17g ", pow(x*x + y*y, 0.5));
  }
  printf("\n");
  return(0);
}
#endif

/*
 * $ gcc kamadorueda.c -lm -march=native -O3 -o kamadorueda
 * $ cat DATA.lst | ./kamadorueda
 *   0 4.582575694934869 3.6055512754543941 4 3.4641016151333162
 *   6.2449979983959363 2.6457513110515145 1.7320508075666579
 *   1.7320508075599998 0.99999999999615607 5.2915026221262753
 *   0.99999999999615607 5.2915026221262753 0.99999999999615607
 *   2.9999999999884679 0.99999999999615607 2 1.7320508075666581
 *   2.6457513110631377 6.5574385242808972 3.6055512754543941
 *   3.4641016151333157 0 1.7320508075666579
 */
