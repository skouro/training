-- | Codeabbey 78: Bézier Curves
-- | http://www.codeabbey.com/index/task_view/bezier-curves
-- | **** Compilation
-- | $ ghc -dynamic raballestasr.hs
-- | [1 of 1] Compiling Main             ( raballestasr.hs, raballestasr.o )
-- | Linking raballestasr ...
-- | **** test run (with easy DATA.lst)
-- | $ ./raballestasr
-- | "222 246 211 212 234 254 19 214 172 234 244 254 175 213"
-- | **** Linter output
-- | $ hlint raballestasr.hs
-- | No hints
bezier :: Double -> (Double,Double) -> (Double,Double) -> (Double,Double)
bezier alf (x1,y1) (x2,y2) = (x1 + alf*(x2-x1), y1 + alf*(y2-y1))

recurr :: Double -> [(Double,Double)] -> [(Double,Double)]
recurr alf points
    | length points == 1 = points
    | otherwise = recurr alf (zipWith (bezier alf) (init points) (tail points))

main = do
    input <- readFile "DATA.lst"
    let n = read (last $ words $ head $ lines input) ::Double
    let step = (n-1)**(-1)
    let alphas = map (\x -> step*x) [0..n]
    let testpoints = map (tuplify . map (\ x -> read x :: Double) . words)
         (tail $ lines input) where tuplify [x,y] = (x,y)
    print $ unwords $ map mostr (concatMap (`recurr` testpoints) alphas)
        where mostr (x,y) = show (round x) ++ " " ++ show (round y)
