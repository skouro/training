#!/usr/bin/env python2.7
'''
$ pylint jicardona.py
No config file found, using default configuration

------------------------------------
Your code has been rated at 10.00/10
'''

TOTAL = 0
IPS = []

with open('DATA.lst', 'r') as data:
    TOTAL = data.readline().strip()
    for line in data:
        IPS.append(line.strip())

COUNTRIES = []

with open('db-ip.txt', 'r') as db_ip:
    for ip in db_ip:
        COUNTRIES.append(ip.strip().split())

SOLUTION = []

for ip in IPS:
    LEFT = 0
    RIGTH = len(COUNTRIES) - 1
    while LEFT <= RIGTH:
        middle = int((LEFT + RIGTH) / 2)
        bottom = COUNTRIES[middle]
        top = COUNTRIES[middle + 1]
        if int(bottom[0], 36) <= int(ip, 36) < int(top[0], 36):
            if int(bottom[0], 36) + int(bottom[1], 36) >= int(ip, 36):
                SOLUTION.append(bottom[2])
            else:
                SOLUTION.append('unknown')
            break
        if int(bottom[0], 36) < int(ip, 36):
            LEFT = middle + 1
        else:
            RIGTH = middle - 1

print ' '.join(SOLUTION)

# pylint: disable=pointless-string-statement

'''
$ python jicardona
GB PK FR AU ZA US AU CN BH AL PL UG ... CH DE FR RU HR US BR US RU JP DE US FR
'''
