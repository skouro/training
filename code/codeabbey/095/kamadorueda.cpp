/*
Compiling with g++
$ g++ -o kedavamaru -std=c++14 -march=native -O3 kedavamaru.cpp

Lintint nazi with cppcheck
$ cppcheck -I '/usr/include/c++/5.4.0' --enable=all --force kedavamaru.cpp
Checking kedavamaru.cpp ...
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;
using dbl = double;
using uint = unsigned int;
using vdbl = vector<double>;
using mdbl = vector<vdbl>;

struct line {
  vdbl C;
  dbl r;
};

line lin_reg(dbl order, dbl n, vdbl const &Y, vdbl const &X) {
  line reg;

  dbl m = order + 1;
  vdbl T(2 * order + 1, 0);
  mdbl M(m, vdbl(m + 1, 0));

  T[0] = n;
  for (uint i = 0; i < n; ++i) {
    for (uint j = 1; j < T.size(); ++j) {
      T[j] += pow(X[i], j);
    }
    for (uint j = 0; j < m; ++j) {
      M[j][m] += pow(X[i], j) * Y[i];
    }
  }
  for (uint i = 0; i < m; ++i) {
    for (uint j = 0; j < m; ++j) {
      M[i][j] = T[i + j];
    }
  }

  for (uint i = 0; i < m; ++i) {
    // Search for maximum in this column
    dbl maxel = abs(M[i][i]);
    uint maxRow = i;
    for (uint k = i + 1; k < m; ++k) {
      dbl absmki = abs(M[k][i]);
      if (absmki > maxel) {
        maxel = absmki;
        maxRow = k;
      }
    }

    // Swap maximum row with current row (column by column)
    for (uint k = i; k <= m; ++k) {
      dbl tmp = M[maxRow][k];
      M[maxRow][k] = M[i][k];
      M[i][k] = tmp;
    }

    // Make all rows below this one 0 in current column
    for (uint k = i + 1; k < m; ++k) {
      dbl c = -M[k][i] / M[i][i];
      for (uint j = i; j <= m; j++) {
        if (i == j) M[k][j] = 0;
        else M[k][j] += c * M[i][j];
      }
    }
  }

  reg.C = vdbl(m, 0);
  // Solve equation Ax=b for an upper triangular matrix A
  for (uint i = m - 1; i >= 0; i--) {
    reg.C[i] = M[i][m] / M[i][i];
    if (i == 0) break;
    for (uint k = i - 1; k >= 0; --k) {
      M[k][m] -= M[k][i] * reg.C[i];
      if (k == 0) break;
    }
  }

  return reg;
}

int main() {
  ifstream file("DATA.lst");
  if (!file) return 1;

  uint iyear, lyear;
  file >> iyear >> lyear;
  uint n = lyear-iyear+1;
  vdbl d(n);
  vdbl p(n);
  for (uint y = iyear, i = 0; y <= lyear; ++y, ++i) {
    string tmp;
    file >> tmp >> d[i] >> p[i];
  }

  line reg = lin_reg(1, n, p, d);
  cout.precision(9);
  cout << reg.C[1] << " " << reg.C[0] << endl;
}

/*
assuming 'DATA.lst' is on the same folder as 'kedavamaru'
$ kedavamaru
2.10020655 91.9065358
*/
