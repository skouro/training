# $ julia
#
#   _       _ _(_)_     |  A fresh approach to technical computing
#  (_)     | (_) (_)    |  Documentation: https://docs.julialang.org
#   _ _   _| |_  __ _   |  Type "?help" for help.
#  | | | | | | |/ _` |  |
#  | | |_| | | | (_| |  |  Version 0.6.4 (2018-07-09 19:09 UTC)
# _/ |\__'_|_|_|\__'_|  |  Official http://julialang.org/ release
#|__/                   |  x86_64-w64-mingw32
# $ using Lint
# $ length(lintfile("actiradob.jl"))
# 0

function chess()
  board = [["R","N","B","Q","K","B","N","R"],
           ["P","P","P","P","P","P","P","P"],
           ["-","-","-","-","-","-","-","-"],
           ["-","-","-","-","-","-","-","-"],
           ["-","-","-","-","-","-","-","-"],
           ["-","-","-","-","-","-","-","-"],
           ["p","p","p","p","p","p","p","p"],
           ["r","n","b","q","k","b","n","r"]]
  return board
end

function col2row(col)
  if col == 'a' return 1
  elseif col == 'b' return 2
  elseif col == 'c' return 3
  elseif col == 'd' return 4
  elseif col == 'e' return 5
  elseif col == 'f' return 6
  elseif col == 'g' return 7
  else return 8 end
end

function validator(b, move)
  valid = false
  iR = parse(Int64,move[2])
  iC = col2row(move[1])
  fR = parse(Int64,move[4])
  fC = col2row(move[3])
  #println(iR," ",iC," ",fR," ",fC)
  if b[iR][iC] == "P"
    if [fR,fC] == [iR+1,iC+1] || [fR,fC] == [iR+1,iC-1]
      if uppercase(b[fR][fC]) != b[fR][fC]
        b[fR][fC] = b[iR][iC]
        b[iR][iC] = "-"
        valid = true
      end
    end
    if [fR,fC] == [iR+2,iC] && iR == 2
      if b[fR][fC] == "-" && b[fR-1][fC] == "-"
        b[fR][fC] = b[iR][iC]
        b[iR][iC] = "-"
        valid = true
      end
    end
    if [fR,fC] == [iR+1,iC]
      if b[fR][fC] == "-"
        b[fR][fC] = b[iR][iC]
        b[iR][iC] = "-"
        valid = true
      end
    end
  elseif b[iR][iC] == "p"
    if [fR,fC] == [iR-1,iC+1] || [fR,fC] == [iR-1,iC-1]
      if uppercase(b[fR][fC]) == b[fR][fC]
        b[fR][fC] = b[iR][iC]
        b[iR][iC] = "-"
        valid = true
      end
    end
    if [fR,fC] == [iR-2,iC] && iR == 7
      if b[fR][fC] == "-" && b[fR+1][fC] == "-"
        b[fR][fC] = b[iR][iC]
        b[iR][iC] = "-"
        valid = true
      end
    end
    if [fR,fC] == [iR-1,iC]
      if b[fR][fC] == "-"
        b[fR][fC] = b[iR][iC]
        b[iR][iC] = "-"
        valid = true
      end
    end
  else
    b[fR][fC] = b[iR][iC]
    b[iR][iC] = "-"
    valid = true
  end
  return b,valid
end

function testValidator()
  answer = ""
  open("DATA.lst") do f
    ntestCases = parse(Int32,readline(f))
    for i = 1:ntestCases
      moves = split(readline(f))
      incorrect = 0
      b = chess()
      for j = 1:length(moves)
        b, valid = validator(b,moves[j])
        if !valid
          incorrect = j
          break
        end
      end
      answer = string(answer," ",incorrect)
    end
  end
  answer = answer[2:length(answer)]
  return answer
end

testValidator()
# $ include("actiradob.jl")
# "6 4 0 0 6 0 6 4 4 3 4 4 2"
