#include <iostream>
#include <string.h>

using namespace std;

void read_vals(int num, int *isles){
    int i,offset=0;
    cin.ignore();
    for(i=0;i<num;i++){
        char series[100];
        cin.getline(&series[0],100);
        int j;
        for(j=0;j<strlen(series);j+=2){
            if((series[j+1]==' ')||(series[j+1]=='\0')){
                *(isles+offset)=(int)series[j]-48;
                offset+=1;
            }
            else{
                *(isles+offset)=((int)series[j]-48)*10+((int)series[j+1]-48);
                offset+=1;
                j++;
            }
        }
        offset=50*(i+1);
    }
}

int best_path(int *isles,int sum,int max){
    sum+=*isles;
    if(sum>max){
        max=sum;
    }
    if(*(isles+2)!=0){
        max=best_path(isles+2,sum,max);
    }
    if(*(isles+3)!=0){
        max=best_path(isles+3,sum,max);
    }
    return max;
}

int main(void){
    int num,i;
    cin>>num;
    int isles[num][50]; fill(isles[0],isles[0]+num*50,0);
    read_vals(num,&isles[0][0]);
    for(i=0;i<num;i++){
        int sum=0,max=0,offset=50*i;
        max=best_path(&isles[0][0]+offset,sum,max);
        cout<<max<<' ';
    }
}
