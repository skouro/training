#include <iostream>
#include <string.h>

using namespace std;

void read_vals(int num, int *data){
    cin.ignore();
    for(int i=0;i<num;i++){
        char series[5]; int offset=2*i;
        cin.getline(&series[0],5);
        for(int j=0;j<strlen(series);j+=2){
            if((series[j+1]==' ')||(series[j+1]=='\0')){
                *(data+offset)=(int)series[j]-48;
                offset+=1;
            }
            else{
                *(data+offset)=((int)series[j]-48)*10+((int)series[j+1]-48);
                offset+=1;
                j++;
            }
        }
    }
}

int main(void){
    int num;
    cin>>num;
    int data[num][2];
    read_vals(num,&data[0][0]);
    for(int i=0;i<num;i++){
        cout<<data[i][0]*(data[i][1]-1)<<' ';
    }
    return 0;
}
