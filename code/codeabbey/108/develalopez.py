'''
Problem #108 Star Medals
$ pylint develalopez.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 develalopez.py
'''

from __future__ import print_function


def gems(lines, steps):
    '''
    Function that returns the number of intersections generated by a polygon of
    N lines joined each T steps.
    '''
    if steps == 1:
        return 0
    return (steps - 1) * lines


def main():
    '''
    Main function that reads and solves the problem's input.
    '''
    d_file = open("DATA.lst", "r")
    cases = int(d_file.readline())
    answers = []
    for _ in range(cases):
        lines, steps = map(int, [int(x) for x in d_file.readline().split()])
        answers.append(gems(lines, steps))
    print(" ".join(str(x) for x in answers))


main()

# pylint: disable=pointless-string-statement

'''
$ py develalopez.py
20 51 27 102 32 57 15 11 17
'''
