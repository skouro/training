/*
$ rustfmt rafa1894.rs #linting
$ rustc rafa1894.rs #compilation
*/

use std::env;
use std::fs;

fn deciph(c: char, k: u32) -> char {
    let ciph = c;
    let key = k;
    let mut inv = 0;
    let mut y = 0;
    let mut z = 0;
    let vec = vec![
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    ];
    if c == '.' {
        return '.';
    }
    if c == '\n' {
        return ' ';
    }
    for _i in vec.iter() {
        if vec[y] == ciph {
            inv = y;
            break;
        }
        y += 1;
    }
    if (inv as i32 - key as i32) < 0 {
        z = -(inv as i32 - key as i32);
        return vec[26 - z as usize];
    }
    return vec[inv - key as usize];
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file = &args[1];
    let inputstr = fs::read_to_string(file).expect("Error");
    let mut x = 0;
    let mut k = 0;
    for i in inputstr.chars() {
        if x == 0 || x == 1 {
            x += 1;
            continue;
        }
        if x == 2 {
            x += 1;
            k = i.to_digit(10).unwrap();
            continue;
        }
        if x == 3 {
            x += 1;
            continue;
        }
        if i == ' ' {
            print!("{}", i);
            continue;
        }
        print!("{}", deciph(i, k));
    }
}
/*
$ ./rafa1894 DATA.lst
CALLED IT THE RISING SUN FOUR SCORE AND SEVEN YEARS AGO THE ONCE AND
 FUTURE KING. A DAY AT THE RACES. MET A WOMAN AT THE WELL. THE DEAD
 BURY THEIR OWN DEAD THAT ALL MEN ARE CREATED EQUAL. AS EASY AS LYING.
 AND SO YOU TOO BRUTUS. THE SECRET OF HEATHER ALE WHO WANTS
 TO LIVE FOREVER.
*/
