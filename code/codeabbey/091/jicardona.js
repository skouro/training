#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global arguments, print */

function stackY(grid, down, row = 0, sub = 1, column = 0) {
  if (row > grid.length - 2) {
    return grid;
  }
  if (sub > grid.length - 1) {
    return stackY(grid, down, row, row + 1, column + 1);
  }
  if (column > grid.length - 1) {
    return stackY(grid, down, row + 1, sub + 1);
  }
  const currRow = down ? grid.length - (row + 1) : row;
  const subRow = down ? grid.length - (sub + 1) : sub;
  if (grid[currRow][column] === '-' && grid[subRow][column] !== '-') {
    const swap = grid[subRow][column];
    const current = Object.assign([ ...grid[currRow] ], { [column]: swap });
    const previous = Object.assign([ ...grid[subRow] ], { [column]: '-' });
    const newGrid =
      Object.assign([ ...grid ], { [currRow]: current, [subRow]: previous });
    return stackY(newGrid, down, row, row + 1, column + 1);
  }
  return stackY(grid, down, row, sub + 1, column);
}

function moveY(grid, down = true, row = 0, column = 0) {
  if (row > grid.length - 2) {
    return stackY(grid, down);
  }
  if (column > grid.length - 1) {
    return moveY(grid, down, row + 1);
  }
  const currRow = down ? grid.length - (row + 1) : row;
  const prevRow = down ? currRow - 1 : currRow + 1;
  if (
    grid[currRow][column] === grid[prevRow][column] &&
      grid[currRow][column] !== '-') {
    const add = grid[currRow][column] + grid[prevRow][column];
    const current = Object.assign([ ...grid[currRow] ], { [column]: add });
    const previous = Object.assign([ ...grid[prevRow] ], { [column]: '-' });
    const newGrid =
      Object.assign([ ...grid ], { [currRow]: current, [prevRow]: previous });
    return moveY(newGrid, down, row, column + 1);
  }
  return moveY(grid, down, row, column + 1);
}

function stackX(grid, right, column = 0, sub = 1, row = 0) {
  if (column > grid.length - 2) {
    return grid;
  }
  if (sub > grid.length - 1) {
    return stackX(grid, right, column, column + 1, row + 1);
  }
  if (row > grid.length - 1) {
    return stackX(grid, right, column + 1, sub + 1);
  }
  const currCol = right ? grid.length - (column + 1) : column;
  const subCol = right ? grid.length - (sub + 1) : sub;
  if (grid[row][currCol] === '-' && grid[row][subCol] !== '-') {
    const swap = grid[row][subCol];
    const combine =
      Object.assign([ ...grid[row] ], { [currCol]: swap, [subCol]: '-' });
    const newGrid = Object.assign([ ...grid ], { [row]: combine });
    return stackX(newGrid, right, column, column + 1, row + 1);
  }
  return stackX(grid, right, column, sub + 1, row);
}

function moveX(grid, right = true, column = 0, row = 0) {
  if (column > grid.length - 2) {
    return stackX(grid, right);
  }
  if (row > grid.length - 1) {
    return moveX(grid, right, column + 1);
  }
  const currCol = right ? grid.length - (column + 1) : column;
  const prevCol = right ? currCol - 1 : currCol + 1;
  if (grid[row][currCol] === grid[row][prevCol] && grid[row][currCol] !== '-') {
    const add = grid[row][currCol] + grid[row][prevCol];
    const combine =
      Object.assign([ ...grid[row] ], { [currCol]: add, [prevCol]: '-' });
    const newGrid = Object.assign([ ...grid ], { [row]: combine });
    return moveX(newGrid, right, column, row + 1);
  }
  return moveX(grid, right, column, row + 1);
}

function tileCounter(grid, moves) {
  if (moves.length !== 0) {
    switch (moves[0]) {
      case 'U': {
        const upGrid = moveY(grid, false);
        return tileCounter(upGrid, moves.slice(1));
      }
      case 'D': {
        const downGrid = moveY(grid);
        return tileCounter(downGrid, moves.slice(1));
      }
      case 'L': {
        const leftGrid = moveX(grid, false);
        return tileCounter(leftGrid, moves.slice(1));
      }
      default: {
        const rightGrid = moveX(grid);
        return tileCounter(rightGrid, moves.slice(1));
      }
    }
  }
  const set = grid.reduce((arr = [], current) => arr.concat(current));
  const altArr = set.filter((current) => current !== '-');
  const max = Math.max(...altArr);
  const tiles =
    Array.from(new Array(Math.log2(max)),
      (curr, index) => Math.pow(2, index + 1));
  const count =
    tiles.map((current) => altArr.filter((curr) => curr === current).length);
  return count;
}

/* eslint-disable fp/no-arguments */
const [ input ] = arguments;
/* eslint-enable fp/no-arguments */

const lines = input.split(/\n/);

const last = -1;

const grid =
  lines.slice(0, last).map((current) => current.split(/\s/).map(Number));
const [ moves ] = lines.slice(last);

/* eslint-disable fp/no-unused-expression*/
print(tileCounter(grid, moves.split(/\s/)).join(' '));
/* eslint-enable fp/no-unused-expression*/

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
2 4 1 1
*/
