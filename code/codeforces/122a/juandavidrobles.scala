object JuanDavidRobles122A {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn
    import scala.util.control.Breaks._

    var number: Int = Integer.parseInt(StdIn.readLine())

    var string: String = ""
    var out: String = "NO"

    breakable{
      string = String.valueOf(number)
      string = string.replace("4","")
      string = string.replace("7","")
      if (string.equals("")){
        out = "YES"
        break()
      }

      for (i <- 4 to number){

        string = String.valueOf(i)
        string = string.replace("4","")
        string = string.replace("7","")
        if (string.equals("") && number%i == 0){
          out = "YES"
          break()
        }
      }
    }

    println(out)

  }
}
