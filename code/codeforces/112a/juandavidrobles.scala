object JuanDavidRobles112A {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn
    var str1 : String = StdIn.readLine()
    var str2 : String = StdIn.readLine()

    if (str1.equalsIgnoreCase(str2)){
      println("0")
      return
    }

    str1 = str1.toUpperCase()
    str2 = str2.toUpperCase()

    if (str1.compareTo(str2) > 0){
      println("1")
    } else {
      println("-1")
    }
  }
}
