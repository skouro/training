object JuanDavidRobles133A {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn
    import scala.util.control.Breaks._

    val string: String = StdIn.readLine()
    var conditional: Boolean = false

    breakable{
      for (i <- 0 until string.length){
        string.charAt(i) match {
          case 'H' => conditional = true
            break()
          case 'Q' => conditional = true
            break()
          case '9' => conditional = true
            break()
          /*case '+' => conditional = true
            break()*/
          case _ =>
        }
      }
    }

    if (conditional){
      println("YES")
    } else {
      println("NO")
    }
  }
}
