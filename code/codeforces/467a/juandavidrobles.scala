import java.io.BufferedReader

object JuanDavidRobles467A {
  def main(args: Array[String]): Unit = {

    import java.util.Scanner
    import java.io.InputStreamReader

    val streamReader: InputStreamReader = new InputStreamReader(System.in)
    val scanner: Scanner = new Scanner(new BufferedReader(streamReader))

    val n: Int = Integer.parseInt(scanner.next())
    var capacity: Int = 0
    var people: Int = 0
    var count: Int = 0

    for (i <- 0 until n){
      people = Integer.parseInt(scanner.next())
      capacity = Integer.parseInt(scanner.next())
      if (capacity - people >= 2){
        count = count + 1
      }
    }

    println(count)
  }
}
