/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 15, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
ADVERTENCIA:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.io.*;
import java.util.*;
import java.text.DecimalFormat;
import java.util.Properties;
import java.net.URL;
import static java.lang.System.out;

class Bpmcount {
 private static DecimalFormat df = new DecimalFormat("0.00");

 public static void main(String args[]) throws Exception {
  URL path = ClassLoader.getSystemResource("DATA.lst");
  InputStream input = new FileInputStream(path.getFile());
  Properties prop = new Properties();
  prop.load(input);

  Scanner scanner = new Scanner(System.in);
  String readString = scanner.nextLine();
  int count = 0;
  Long t = new Long(0);

  if (readString.equals("")) {
   t = System.currentTimeMillis();
   out.println(prop.getProperty("msgread") + 0);
   count++;
   while (readString != null) {
    readString = scanner.nextLine();
    if (readString.equals("") && count < 8) {
     count++;
     out.println("key read at:" +
      (System.currentTimeMillis() - t) / 1000.0);
    } else {
     readString = null;
     long elap = System.currentTimeMillis() - t;
     out.println("key count:" + count);
     out.println("bpm per minute:" +
      df.format((60.0 * count) / (elap / 1000.0)));
    }
   }
  }
 }
}
/*
$ java karlhanso82
bpm count
$
*/

