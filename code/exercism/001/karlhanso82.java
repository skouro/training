/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 02, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
ADVERTENCIA:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.lang.System.*;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.net.URL;

class Hellow {

 public static void main(String args[]){
  URL path = ClassLoader.getSystemResource("DATA.lst");
  try (InputStream input = new FileInputStream(path.getFile())){
   Properties prop = new Properties();
   prop.load(input);
   System.out.println(prop.getProperty("msg"));
  } catch (IOException e) {
   e.printStackTrace();
  }
 }
}
/*
$ java karlhanso82
hello World
$
*/

