package esmejia;

import javax.swing.JOptionPane;
import java.util.Scanner;

/**
 * @author esteban
 */
final class Esmejia {

    /**
     * 0.
     */
    private static final int ZERO = 0;

    /**
     * Empty constructor.
     *
     * @param empty
     */
    private Esmejia() {
    }

    /**
     *
     * @param args the command line arguments
     * @see main
     * @kind of traingle
     */
    public static void main(final String[] args) {
        Scanner p;
        p = new Scanner(System.in);
        System.out.println("add side 1:");
        int a = p.nextInt();
        System.out.println("add side 2:");
        int b = p.nextInt();
        System.out.println("add side 3:");
        int c = p.nextInt();
        if (a > ZERO && b > ZERO && c > ZERO) {
            if (a == b && b == c) {
                System.out.println("equilateral triangle");
            } else {
                if (a != b || c != b || a != c) {
                    System.out.println("escalene triangle");
                }
                if (a == b || a == c || c == b) {
                    JOptionPane.showMessageDialog(null, "isosceles triangle");
                }
            }
        } else {
            System.out.println("!!!¡¡¡###ERROR###!!!¡¡¡");
        }
    }
}
