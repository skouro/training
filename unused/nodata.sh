#!/bin/bash

echo "### Retos de programación sin set de datos"

find challenges/codeeval challenges/codeabbey \
    -type d '!' \
        -exec test -e "{}/DATA.lst" ';' \
        -print
