## Version 2.0
## language: en

Feature: Root Me - Crypto - File - PKZIP
  Site:
    Root Me
  Category:
    Crypto
  Challenge:
    File - PKZIP
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>                         |
    | Ubuntu          | 18.04.1 LTS (amd64)               |
    | Google Chrome   | 70.0.3538.77 (64-bit)             |
    | John the Ripper | 1.8.0.13-jumbo-1-bleeding-e582498 |

  Machine information:
    Given I'm accesing the challenge page
    And the problem statement is provided
    """
    A protected ZIP file, you have to find what’s inside.
    """
    And a ZIP file is provided
    """
    |-- ch5.zip
      |-- readme.txt  111Bytes   <- password protected
    """

  Scenario: Success: Brute force attack
    Given The problem is to read what's inside of readme.txt
    And it is password protected
    Then I create a John-the-Ripper hash file from a protected zip file
    """
    $ ./zip2john "${somepath1}/ch5.zip" > "${somepath2}/hash.txt"
    ver 2.0 efh 5455 efh 7855 ch5.zip/readme.txt
      PKZIP Encr: 2b chk, TS_chk, cmplen=99, decmplen=111, crc=EE166206
    """
    When I crack the created hash file
    """
    $ ./john "${somepath2}/hash.txt"
    Using default input encoding: UTF-8
    Loaded 1 password hash (PKZIP [32/64])
    ...
    14535            (ch5.zip/readme.txt)
    ...
    Session completed
    """
    Then I discover the password is "14535"
    When I unzip the protected zip file
    """
    $ unzip "${somepath1}/ch5.zip" -d "${somepath1}"
    Archive:  "${somepath1}/ch5.zip"
      [${somepath1}/ch5.zip] readme.txt password: 14535
      inflating: /home/fluid/Desktop/readme.txt
    """
    And I read the content of "readme.txt"
    """
    $ cat "${somepath1}/readme.txt"
    Use ZIP password to validate this challenge.
    Utiliser le mot de passe de l'archive pour valider le challenge.
    """
    And I use "14535" as the challenge solution
    Then I solve the challenge
