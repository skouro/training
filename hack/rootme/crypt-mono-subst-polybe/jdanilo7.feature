## Version 2.0
## language: en

Feature:  Cryptanalysis - Root Me
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
  User:
    jdanilo7
  Goal:
    Decrypt a monoalphabetic substittion Polybius square ciphered message

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |    60.2.0esr    |
  Machine information:
    Given The challenge url
    """
    https://www.root-me.org/en/Challenges/Cryptanalysis/Monoalphabetic-
    substitution-Polybe
    """
    And The challenge statement
    """
    A strange person contact you after purchasing a parchment of weird origin
    ... He count on your quick wit to decrypt this message ! You will need to
    deploy all your cryptanalyse’s faculties.
    """
    Then I am asked to decrypt the message.

  Scenario: Fail: Use the usual alphabetical order to decrypt the message
    Given I click the "Start the challenge" button
    Then I am redirected to a webpage with paragraphs like this
    """
    b3a3d1 c2b1e3d4d3d1 a4e5c5b1e3c2a3d1 b3a4b4e1e3b1a3d1 e3e5 b3a4b1e1d1
    b2e5b4e3d2d4 d1a3b4c3c4a3d4a1 d1a4c4d2d3a3d1 b3a4d4d1d2d3a3b1a3d1
    d1a3c4a4d4 c4a3d1 c4a4d2d1 d3a3 b3a3a1a1a3 d4e3a1e5b1a3 c1e5d2
    d3a3a1b1e5d2a1 c1e5d2 b1e3e1e1a3c4c4a3 a1a4e5a1a3d1 b3b2a4d1a3d1 e3e5
    d4a3e3d4a1 d3a4d4a1 a3c4c4a3 c4a3d1 e3 a1d2b1a3a3d1
    """
    And I conclude the message is encoded using a Polybius table
    And the rows are indexed with the letters a-e
    And the columns are indexed with the numers 1-5
    Given I build a Polybius table with the english alphabet like this
    """
    Frech alphabet table
      |   | 1 | 2 | 3 | 4 | 5 |
      | A | a | b | c | d | e |
      | B | f | g | h | i | k |
      | C | l | m | n | o | p |
      | D | q | r | s | t | u |
      | E | v | w | x | y | z |
    """
    And I try to decrypt the message using it
    Then I fail
    Given I try to rearrange the letters in several ways
    And I try to decrypt the message using the new tables
    And I don't get any decrypted message.

  Scenario: Success: Use frequency analysis to decode the first letters
    Given the challenge message
    Then I analyze it further
    And I notice there are many words with only one letter
    And I conclude the encoded message is written in french
    And I also notice the last line has a different structure
    """
    e1e3d1d1 / b4a4a1 d3a3 e1e3d1d1a3 : b3e1e3e3a4e5b1c3b5e1a2e1c2e2a4
    """
    And I conclude it contains the password
    Given I look for the most common letters in french
    Then I find out they are 'E', 'A', 'I', 'S', 'T', 'N' and 'R'
    Given I replace the most common letter-number pairs
    Then the last line becomes
    """
    e1ASS / b4a4a1 d3E e1ASSE : b3e1e3e3a4e5b1c3b5e1a2e1c2e2a4
    """
    And I guess it is
    """
    PASS / b4a4a1 d3E PASSE : b3e1e3e3a4e5b1c3b5e1a2e1c2e2a4
    """
    Given I find more french patterns and words
    Then I get the following Polybius table
    """
    Final table
      |   | 1 | 2 | 3 | 4 | 5 |
      | A | t | j | e | o |   |
      | B | r | h | c | m | x |
      | C | q | g | b | l | v |
      | D | s | i | d | n | y |
      | E | p | f | a |   | u |
    """
    And it is enough to decode the password
    """
    PASS / MOT DE PASSE : cpaaourbxpjpgfo
    """
    Then I validate it on the challenge website
    And it is accepted
