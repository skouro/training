## Version 2.0
## language: en

Feature: hackburger
  Site:
    hackburguer
  User:
    charlie517 (wechall)
  Goal:
    get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | 4.15.0-45   |
    | Firefox         | 65.0.1      |
  Machine information:
    Given I am accessing the hacktheburger website
    And PHP version 5.6.39-0
    And is running on Ubuntu 4.9.0-7

  Scenario: Success:read flag.php
    Given the web page gives me a PHP code with non actual use to the challenge
    And I input '/?number=eval(system('ls -aR'))' in browser's search bar
    Then I can see a tree of directories with 'flag.php' file in them
    Then I try to read the file by doing
    """
    ?number=eval(system('cat where/is/the/flag/i/am/looking/for/flag.php'))
    """
    And see that nothing changes
    Then I inspect the webpage source code and see this (evidence)[evidence.png]
    And I conclude that worked
    And solved the challenge
    And I caught the flag
