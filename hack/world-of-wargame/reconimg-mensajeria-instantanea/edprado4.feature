Feature: Solve Mensajeria instantanea challenge
  from site World of Wargame
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS


Scenario: Challenge solved
  Given The compilation of instant messaging icons
  When I download the image
  And I separate each icon by cropping them
  And I load each cropped image into google images
  Then I find the name of each program
  And I solve the challenge
