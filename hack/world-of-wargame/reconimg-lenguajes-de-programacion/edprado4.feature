Feature: Solve Programming languages challenge
  from site World of Wargame
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: First failed attempt
  Given A mosaic with different pictures of people
  When I download the image
  And I demosaic it into different pictures
  Then I try to google each image by separate
  But Google doesn't recognize some pictures
  And I fail the challenge
  But I understand I have to change my search criteria

Scenario: Challenge solved
  Given My previous failed attempt
  And A clue about programming languages
  When I search in google for "programming languages"
  And I find a wiki about most used programming languages
  When I search images of their creators names
  And I compare them with the pictures of the challenge
  Then I find the remaining names
  And I solve the challenge
