Feature: Solve Prime Time challenge
  from site Rankk
  logged as Edprado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS
  And I have Eclipse IDE for Java Developers

Scenario: Challenge solved
  Given The challenge presented
  When I realize the ammount of primes to add is dynamic
  Then I write a script in java to make the sum
  When I get the sum of the n first primes
  Then I use that value as a password
  And I solve the challenge
