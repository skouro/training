# language: en

Feature: Solve A compromising image
  From site rankk.org
  From Level 02 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given a Python web site with a login form
  """
  URL: https://www.rankk.org/challenges/a-compromising-image.py
  Message: Your friend Melissa is dying to see the first of the two
  photos on this directory. Too bad it is protected...

  Objetive: See image two
  Evidence: Hyperlink that opens a file directory
  """

Scenario: Analyzing the problem
The page shows a sentence and a text field
  Then I open the link that has the statement
  """
  http://www.rankk.org/challenges/photos/
  """
  Then I try to open image one and I do not have access
  """
  Forbidden

  You don't have permission to access /challenges/photos/photo1.jpg
  on this server.
  """
  Then I see that the directory has a file with thumbnails of images
  """
  Thumbs.ds
  """
  And I download it to extract the thumbnails

Scenario: Accessing the thumbnails
I have a Thumbs file in which you can have the solution
  Then I am ready to download the following program:
  """
  Ubuntu
  Name of the program: vinetto
  Installation command: sudo apt-get install vinetto

  It asks for our password to install and proceed to install
  the program to extract the thumbnails
  """
  Then we open the terminal from the location of the Thumbs.db file
  And we write the following command:
  """
  vinetto Thumbs.db -o ./

  Complete line of the terminal:
  santi@santi-HP-Notebook:~/Descargas$ vinetto Thumbs.db -o ./

  Result in console:

  ** Warning: Cannot find "Image" module.
  Vinetto will only extract Type 2 thumbnails.

   Root Entry modify timestamp : Thu Aug 21 18:30:18 2008

   0001   Thu Mar 13 07:37:02 1997   photo2.jpg
   0002   Thu Aug 21 18:09:54 2008   photo1.jpg

  2 Type 2 thumbnails extracted to .//
  """
  Then I realize that the files are hidden
  And I press the following combination to see the extracted folder
  """
  ctrl + h
  """
  Then open the .thumbs folder
  And you find the two images with the possible solution
  Then I see small letters in the image 0002.jpg
  And I open the mentioned image
  Then I increase the zoom of the image to see the letters
  """
  Password: striped hyena

  Taking into account that the tree has a Y shape
  """
  Then I enter the words
  And I solve the challenge
