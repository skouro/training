## Version 1.3.2
## language: en

Feature:
  TOE:
    HTB Curling Machine
  Page name:
    Hack The Box
  CWE:
    CWE-256: Unprotected Storage of Credentials
    CWE-312: Cleartext Storage of Sensitive Information
    CWE-261: Weak Cryptography for Passwords
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Goal:
    Getting a working shell in the machine
  Recommendation:
    Remove secret.txt comment, use a stronger password and store it elsewhere

  Background:
  Hacker's software:
    | <Software name>    | <Version>    |
    | Kali Linux         | 3.30.1       |
    | Firefox ESR        | 60.2.0esr    |
    | OpenVPN            | 2.4.6        |
    | nmap               | 7.70         |
  TOE information:
    Given I am accessing the site 10.10.10.150
    And enter a basic blog site with a login form
    And the server is running Joomla
    And Apache 2.4.29
    And OpenSSH version 7.6p1
    And is running on Linux


  Scenario: Normal use case
  Accesing to the main page of the site.
    Given I access 10.10.10.150
    Then I can see [evidence](landing.png)

  Scenario: Static detection
  There is a comment in the source code leads to the password.
    When I look at the code of index.html
    Then I see the vulnerability: a comment showing the file with a password.
    """
    361 <!-- secret.txt -->
    """
    Then I open the file and see an encrypted string:
    """
    Q3VybGluZzIwMTgh
    """
    When I base64 decode it, it returns Curling2018!
    Then I can conclude that that string may be the password for admin.

  Scenario Outline: Exploitation
  Accessing admin and uploading a PHP shell file.
    Given I read the posts of the blog.
    Then I see can that the author of the first post is Floris
    Then I try and access the site as Floris using Curling2018! as password
    And I gain access to the site as Super User
    Then I see [evidence](upload.png)
    And I try and upload a PHP reverse shell to my IP and port 9001
    Then i execute the following command in my console:
    """
    nc -lvnp 9001
    """
    Then I open the php file in the site.
    And I obtain a shell in a machine with hostname curling as www-data
    Then I can conclude that I have gained access to the machine's files

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-10-31
