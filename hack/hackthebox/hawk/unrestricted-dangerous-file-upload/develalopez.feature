# Version 1.3.1
# language: en

Feature:
  TOE:
    HTB Hawk Machine
  Page name:
    Hack The Box
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Goal:
    Claiming user ownership of the machine.
  Recommendation:
    Restrict insecure methods in PHP code.

  Background:
  Hacker's software:
    | <Software name>    | <Version>    |
    | Kali Linux         | 3.30.1       |
    | Firefox ESR        | 60.2.0esr    |
    | OpenVPN            | 2.4.6        |
  TOE information:
    Given I am accessing the site 10.10.10.102
    And enter a php site that allows me to login
    And the server is running Drupal
    And OpenSSH version 7.6p1
    And is running on Linux

  Scenario: Normal use case
  Accessing to the main page of the site as admin.
    Given I access 10.10.10.120
    Then I can see a simple login form
    And I login using the admin credentials
    Then I see the default Drupal admin panel for a simple blog

  Scenario: Dynamic detection
  The admin has the ability to upload PHP code.
    Given that I access to the configuration panel
    And I go to the file system option
    When I see the following permits [evidence](permiso_php.png)
    Then I can conclude that can upload any PHP exploit.

  Scenario: Exploitation
  Gaining admin permission to upload a reverse PHP shell.
    Given I have the admin logged in
    And I allowed the upload of PHP code files
    Then I allow the administrator to upload PHP code [evidence](formato.png)
    And I create a new article in the blog
    Then I use a reverse shell exploit as the content [evidence](shell_php.png)
    Then I execute the following command in my shell:
    """
    nc -lvnp 9001
    """
    When I go to the main page and open the article
    Then I get a reverse shell working in the machine
    Then I can conclude that I can extract the user.txt flag

  Scenario: Extraction
  Extraction of the user.txt flag
    Given I have a reverse shell working in the machine
    Then I can access /home/daniel and read user.txt
    Then I can conclude that I have user ownership of the machine

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.6/10 (High) - AV:N/AC:H/PR:H/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.3/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/hacktheboox-hawk/unprotected-primary-channel
      Given I obtain admin credentials provided by unprotected-primary-channel
      Then I can chang the configuration settings
      And upload the PHP exploits
