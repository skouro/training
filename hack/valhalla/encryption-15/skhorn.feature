# language: en

Feature: Solve Encryption challenge 15
  From site Valhalla
  From Encryption Category
  With my username Skhorn

  Background:
    Given the fact i have an account on Valhalla site
    And i have Debian 9 as Operating System
    And i have internet access

  Scenario: Succesful Solution
    Given the link to the challenge
    And a long description about Odin and western goddess called Pad
    And an all caps phrase
    And an all caps keyword
    And an indication that the answer is a mix of both
    And i search in google for pad encryption
    And i see the one time pad encryption
    And i use a decoder using the readable word without spaces as plain text
    And i use the keyword as the one-time-pad
    And i encrypt it
    When i see a ciphertext
    And i use it as an answer
    Then i solve the challenge
