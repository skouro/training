# language: en

Feature: Solve Debugging challenge 1
  From site Valhalla
  From Debugging Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using VIM - Vi IMproved 8.0
  And I am using gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516
  Given a web site with a C code to debugg
  And the download link of the source code
  """
  URL: https://halls-of-valhalla.org/beta/challenges/debugging1
  Message: When I run this C code to add the numbers from 1 to
  10 I should get 45, but for some reason I get 10. WTF?
  Objective 1: Find the error and fix it
  Evidence: debugging1.ja
  """

Scenario: Revieweing and patching the code
  Given the source code of the challenge
  And I need to find out why it is not working as expected
  And I compile/execute to see its behavior

  # Compiling & Executing
  """
  $ gcc -o debugging1 debugging1.c
  $ ./debugging1
  Sum: 10
  """

  When I see there is no error
  And check the message indicated before
  And I look at the code
  And I there i see the for-loop at line 8 has an extra character
  """
    6  int sum = 0;
    7  int x = 1;
    8  for(x=1; x<10; x++);
    9  {
  """
  And the for-loop wasn't executing the in block instructions
  And that's why the value expected was not what it throw
  When I remove the extra semicolon
  """
    6  int sum = 0;
    7  int x = 1;
    8  for(x=1; x<10; x++)
    9  {
  """
  And I compile/execute the code
  Then the code behaves as it was intented

  # Compiling & Executing
  """
  $ gcc -o debugging1 debugging1.c
  $ ./debugging1
  Sum: 45
  """
  Then I solve the challenge
