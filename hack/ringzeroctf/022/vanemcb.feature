## Version 2.0
## language: en

Feature: A sound ghost - Steganography - RingZer0 Team Online CTF
  Site:
    RingZer0 Team Online CTF
  Category:
    Steganography
  User:
    vanem_cb
  Goal:
    Find the flag in the audio file

  Background:
  Hacker's software
    | <Software name>  |       <Version>      |
    | Windows          | 10.0.17134.407 (x64) |
    | Chrome           | 70.0.3538.77         |
    | Sonic Visualiser | 3.1.1                |
  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/22
    """
    Then I open the URL with Google Chrome
    And I see the challenge statement
    """
    TAPS team have recorded some ghost sound.
    Can you find what they are saying?
    """
    And I download an audio file
    """
    15d087d9cc86e82b87d0e5ce2cef8583.wav
    """

  Scenario: Fail: Listen the audio
    Given the audio file
    Then I play the audio file
    And I listen something that I don't understand
    And I don't write nothing in the input field to submit the answer
    And I don't solve the challenge

  Scenario: Fail: Edit the audio
    Given the image file
    Then I open a audio edit software
    """
    Pro tools 10 HD
    """
    And I import the audio file
    And I see the sampling rate of the audio file
    And I realize that the sampling rate is lower than the standard
    Then I convert the audio file with the correct sampling rate
    And I play the audio
    But I listen something that I don't understand
    And I don't write nothing in the input field to submit the answer
    And I don't solve the challenge

  Scenario: Succes: Use an analysis tool
    Then I open an analysis audio tool
    """
    Sonic Visualizer 3.1.1
    """
    And I import the audio file
    Then I choose the option to see the signal's spectrogram
    And I see letters in the frequency spectrum [evidence](soundspect.png)
    """
    FLAG-t23jgjr8
    """
    Then I write this words in the input field to submit the answer
    And I solve the challenge
