# language: en

Feature: Solve challenge 40
  From site W3Challs
  From Programming Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using python Python 2.7.14+
  And I am using VIM - Vi IMproved 8.0
  And I am running Burp Suite Community edition v1.7.30
  Given a web site with description, details + a hint
  """
  URL: https://w3challs.com/challs/Prog/Ep4/chuck.php
  Message: You have a total of 5 seconds to solve this challenge
  Details: (a^b) mod c = x, find x
  Objective: Find x in less than 5 seconds
  Evidence: - php site
            - Image contains a,b and c values, 5-digits each
            - Image changes every reload of the site
            - Less than 5 seconds to answer
  """

Scenario: Solving the equation
As stated before, the total time to achieve this, is in less
than 5 seconds, there is no human way to achieve it manually
To overcome the situtation, we gotta script our way to the end
  Given the libraries <beautifulSoup> and <requests> from python
  Then I code a <connect> function:
  """
  Source: w3challs/challenge40/skhorn.py
  109  def connect(self, url, cookie, payload="", proxies=""):
  114    response = requests.post(url,
  115                           cookies=cookie,
  116                           data=payload,
  117                           proxies=proxies,
  118                           verify=False).text
  119    return response
  """

  And I use it, to retrieve data from the web page
  But I need to prepare connection parameters:
  """
  Source: w3challs/challenge40/skhorn.py
  17  url = [ 'https://w3challs.com/challs/Prog/Ep4/chuck.php' ]
  23  cookie = { cookies } => Dict session cookies, better include them all
  """

  And I perform the first request
  Then I get as a response, the entire html of the challenge:
  """
  <br/><br/>
  Dude , <br/>
  Below you can see an equation. <br/>
    ...
    ...
  <img src="number.png.php?id=qk4c9e0udaj0mdk5nqolmlp231 ...
    ...
  """

  And I search the <img[src]> using <beautifulSoup>
  Then I get the needed image
  And using <PIL> and <io> I process it
  And using <pytesseract> I extract the text on it:
  """
  # Proccesing & Extraction
  Source: w3challs/challenge40/skhorn.py
  57  img = Image.open(io.BytesIO(response.content))
  59  text = pytesseract.image_to_string(img)
  """

  Then I need to encode unicode data extracted
  And take each correspondant value
  And store it in <a,b,c> variables
  And this is to solve the equation:

  # Equation
  |---------------|------------|
  | Values given  | Find value |
  |---------------|------------|
  | (a^b) mod c   |  = x       |
  |---------------|------------|

  When I get all values, within 5-digits each
  Then I use python built-in function <pow>
  And I solve the equation, that means I get x <result>
  """
  Source: w3challs/challenge40/skhorn.py
  81  result = pow(int(x_value), int(y_value), int(z_value))
  """
  Then I need to craft another request
  And I include the result value as a payload

  # Payload dict
  """
  Source: w3challs/challenge40/skhorn.py
  85  payload = {'answer': result}
  """

  And I send the new crafted request
  But I don't solve the challenge
  Then I get into another Stage

  # Succesful Response, but...
  """
  Well done your answer is correct!Oh no... Chuck is angry
  because he realized that you are better than him! <br/>
  Hurry up to relaunch this page and "change" your IP to
  escape him! (you will have to send the answer in POST again)
  """

Scenario: "Changing" my IP
One of the classic ways to do this, is by using a Proxy,
not that we need to script it nor use special software,
that's were Requests library will be used again.
  When I search in <Requests> documentation
  And I see there is a special information about proxies
  Then I know I just need to send a proxy dictionary

  # Proxies list example
  |----------------|---------------------|
  | Protocol used  |  Free proxies list  |
  |----------------|---------------------|
  |  HTTPS         | www.sslproxies.org  |
  |----------------|---------------------|
  |  HTTP          | free-proxy-list.net |
  |----------------|---------------------|
  And I look onto the url of the challenge <https://...>
  And I pick a free SSL proxy
  Then I create the proxy payload

  # Proxy payload
  """
  Source: w3challs/challenge40/skhorn.py
  94  proxies = {'https': 'https://151.80.140.233:54566'}
  # We need to specify the protocol used
  # Otherwise the proxy won't work
  """
  And I send the new request
  And again, with all the same parameters
  But with one extra, the proxies payload
  And I get a response from the server
  """
  Well done your answer is correct!Congratz, you solved this challenge! <br/>
  The solution that you have to enter on the site is ... </body>
  """
  When I submit the solution given
  Then I solve the challenge
