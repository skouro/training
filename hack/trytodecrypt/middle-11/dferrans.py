"""
$ pylint dferrans.py #linting
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 dferrans.py #linting

"""
from __future__ import print_function
import time
from selenium import webdriver

ENCRYPTEDSTR = input()
GROUPS = []
READY = []
for pairs_str in range(len(ENCRYPTEDSTR)):
    GROUPS.append(ENCRYPTEDSTR[pairs_str:pairs_str+3])

for str_to_decrypt in GROUPS[0::3]:
    READY.append(str_to_decrypt)

DRIVER = webdriver.Firefox()
DRIVER.get('https://www.trytodecrypt.com/decrypt.php?id=11#headline')

CHARMAP = [" ", "Y", "a", "e", "i", "o", "u", "e", "t", "a", "o", "i",
           "n", "s", "r", "h", "l", "d", "c", "u", "v", "m", "f", "p",
           "g", "w", "y", "b", "k", "x", "j", "q", "z", ".", ",", ";",
           ":", "!", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
           "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p",
           "q", "r", "s", "t", "v", "w", "x", "y", "z", "A", "B", "C",
           "D", "E", "F", "G", "H", "I", "J", "L", "K", "L", "M", "N",
           "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Z"]

ANSWERSTR = "Y"
for str1 in READY:

    if str1 == '378':
        inputfield = DRIVER.find_elements_by_id("comment")
        inputfield[0].clear()
        inputfield[0].send_keys(ANSWERSTR)
        btncrypt = DRIVER.find_element_by_name("encrypt")
        btncrypt.click()
        rst = DRIVER.find_elements_by_class_name("panel-body")
    else:
        inputfield = DRIVER.find_elements_by_id("comment")
        inputfield[0].clear()
        inputfield[0].send_keys(ANSWERSTR + ' ')
        btncrypt = DRIVER.find_element_by_name("encrypt")
        btncrypt.click()
        rst = DRIVER.find_elements_by_class_name("panel-body")
        current = rst[1].text
        rst1 = ""
        while str1 != rst1:

            for char in CHARMAP:
                time.sleep(1)
                inputfield = DRIVER.find_elements_by_id("comment")
                inputfield[0].clear()
                inputfield[0].send_keys(ANSWERSTR + char)
                btncrypt = DRIVER.find_element_by_name("encrypt")
                btncrypt.click()
                resultn = DRIVER.find_elements_by_class_name("panel-body")
                strcompare = resultn[1].text
                if strcompare[-3:] == str1:
                    rst1 = strcompare[-3:]
                    ANSWERSTR += char
                    break


print(ANSWERSTR)
# $python dferrans.py < DATA.lst
# You are very good. Respect!
