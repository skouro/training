## Version 1.0
## language: en

Feature: javaScript-listbrain

  Site:
    http://listbrain.awardspace.biz
  Category:
    javaScript
  User:
    lekriss
  Goal:
    Find the password to capture the flag in the source code
    loaded by the browser

  Background:

  Hacker's software:
    |<version>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given I am accessing the web page using firefox browser
    And http in the port 80
    And Windows 10 as OS
    And ip 181.59.103.56

  Scenario: Success: Searching in the source code trought
  Firefox's elements inspector

    Given I opened the elements in the inspector of firefox
    Then I can see the source code of the page
    Then I try to analize the HTML source in the inspector
    And get the loaded HTML file
    Then I can actually read the flag from the loaded HTML file
    And I conclude that going into the inspector worked
    Then solved the challenge
    And I caught the flag
